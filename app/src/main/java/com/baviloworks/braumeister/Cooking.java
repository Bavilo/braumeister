/*
 * Copyright (C) 2016 Braumeister by BaviloWorks <BaviloWorks@gmail.com>
 *
 * This work is licensed under the Creative Commons
 * Attribution-NonCommercial-ShareAlike 4.0 International License.
 *
 * You may not use this file except in compliance with the License.
 * To view a copy of this license, visit
 *
 *      http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baviloworks.braumeister;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baviloworks.braumeister.ConnectionServices.BluetoothService;
import com.baviloworks.braumeister.ConnectionServices.WiFiService;

import java.util.Objects;

public class Cooking extends Fragment {
    private View view;

    private TextView currentHoppingTime, currentTemperature, targetTemperature, currentTime, targetTime, commentText;
    public static TextView currentHopping, cookingStatus;
    public static CardView temperatures, times, hoppings;
    private RelativeLayout comments;

    public static String stringCurrentHopping, stringTargetTemperature, stringCurrentHoppingTimeHours, stringCurrentHoppingTimeMinutes, stringCurrentHoppingTimeSeconds, stringCurrentTimeHours, stringCurrentTimeMinutes, stringCurrentTimeSeconds, stringTargetCookingHours, stringTargetCookingMinutes;
    public static String hoppingNotify = "";

    public static boolean CookingTVThreadLoop = true;
    public static boolean CookingTVWaitLock = true;
    public static boolean cookingFinished = false;
    public static boolean cookingNotification = false;
    public static boolean hoppingNotification = false;
    private boolean sensorFaultNotificationHG = false;
    private boolean sensorFaultNotificationNG = false;
    public static boolean cookingIsAlive = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.cooking, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.Start:
                start();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem start = menu.findItem(R.id.Start);

        if (MainActivity.hideCookingItems) {
            start.setEnabled(false);
            start.getIcon().setAlpha(128);
        } else {
            start.setEnabled(true);
            start.getIcon().setAlpha(255);
        }

        Objects.requireNonNull(getActivity()).invalidateOptionsMenu();
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.cooking, container, false);

        currentHopping = view.findViewById(R.id.Current_Hopping);
        currentHoppingTime = view.findViewById(R.id.Current_Hopping_Time);
        currentTemperature = view.findViewById(R.id.Current_Temperature);
        targetTemperature = view.findViewById(R.id.Target_Temperature);
        currentTime = view.findViewById(R.id.Current_Cooking_Time);
        targetTime = view.findViewById(R.id.Target_Time);
        cookingStatus = view.findViewById(R.id.Cooking_Status);
        commentText = view.findViewById(R.id.Comment_Text);
        temperatures = view.findViewById(R.id.Temperatures);
        times = view.findViewById(R.id.Times);
        hoppings = view.findViewById(R.id.Hoppings);
        comments = view.findViewById(R.id.comments);

        new Thread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

                while (CookingTVThreadLoop) {
                    if (cookingNotification) {
                        if ((getActivity()) != null) {
                            ((MainActivity) getActivity()).showNotification(MainActivity.cookingID, getString(R.string.Cooking_updateTVTask_Cooking_Finished_Notification_Title), getString(R.string.Cooking_updateTVTask_Cooking_Finished_Notification_Message));
                        }

                        ((MainActivity) getActivity()).sendEmail(SettingsActivity.emailRecipient, getString(R.string.Cooking_updateTVTask_Cooking_Finished_Notification_Title), getString(R.string.Cooking_updateTVTask_Cooking_Finished_Notification_Message));

                        cookingNotification = false;
                    }

                    if (hoppingNotification) {
                        if ((getActivity()) != null) {
                            ((MainActivity) getActivity()).showNotification(MainActivity.hoppingID, stringCurrentHopping + getString(R.string.Cooking_updateTVTask_Hopping_Notification_Title), getString(R.string.Cooking_updateTVTask_Hopping_Notification_Message));
                        }

                        ((MainActivity) getActivity()).sendEmail(SettingsActivity.emailRecipient, stringCurrentHopping + getString(R.string.Cooking_updateTVTask_Hopping_Notification_Title), getString(R.string.Cooking_updateTVTask_Hopping_Notification_Message));

                        hoppingNotification = false;
                    }

                    if (!CookingTVWaitLock) {
                        updateCooking();
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Log.e("Exception", "Could not put thread to sleep - perSecondUpdate");
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        cookingIsAlive = true;

        return view;
    }

    private void start() {
        String disconnectTitle = getResources().getString(R.string.Cooking_Start_Start_Title);
        String disconnectMessage = getResources().getString(R.string.Cooking_Start_Start_Message);

        if (MainActivity.bluetoothConnected || MainActivity.wifiConnected) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(disconnectTitle)
                    .setMessage(disconnectMessage)

                    .setPositiveButton(R.string.General_Notification_Button_Ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (MainActivity.bluetoothConnected) {
                                BluetoothService.sendMessage("K!");
                            } else {
                                WiFiService.sendMessage("K!");
                            }

                            Snackbar.make(view, R.string.Cooking_Start_Started, Snackbar.LENGTH_SHORT).show();
                        }
                    })

                    .setNegativeButton(R.string.General_Notification_Button_Cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {

                        }
                    })

                    .show();
        } else {
            Snackbar.make(view, R.string.Bluetooth_Disconnect_Not_Connected, Snackbar.LENGTH_SHORT).show();
        }
    }

    private void updateCooking() {
        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

                if (SettingsActivity.stringTemperatureSensorHG.equals("-127.00") && !sensorFaultNotificationHG) {
                    currentTemperature.setText(getString(R.string.Mashing_updateTVTask_Mashing_Sensor_Fault));
                    if ((getActivity()) != null) {
                        ((MainActivity) getActivity()).showNotification(MainActivity.sensorID, getString(R.string.Mashing_updateTVTask_Mashing_HG_Sensor_Fault_Notification_Title), getString(R.string.Mashing_updateTVTask_Mashing_HG_Sensor_Fault_Notification_Message));
                    }

                    ((MainActivity) getActivity()).sendEmail(SettingsActivity.emailRecipient, getString(R.string.Mashing_updateTVTask_Mashing_HG_Sensor_Fault_Notification_Title), getString(R.string.Mashing_updateTVTask_Mashing_HG_Sensor_Fault_Notification_Message));

                    sensorFaultNotificationHG = true;
                } else if (!SettingsActivity.stringTemperatureSensorHG.equals(getString(R.string.Mashing_updateTVTask_Mashing_Sensor_Fault)) && !SettingsActivity.stringTemperatureSensorHG.equals("-127.00") ) {
                    currentTemperature.setText(SettingsActivity.stringTemperatureSensorHG + getString(R.string.General_Celcius));

                    sensorFaultNotificationHG = false;
                }

                if (SettingsActivity.stringTemperatureSensorNG.equals("-127.00") && !sensorFaultNotificationNG) {
                    ((MainActivity) getActivity()).showNotification(MainActivity.sensorID, getString(R.string.Mashing_updateTVTask_Mashing_NG_Sensor_Fault_Notification_Title), getString(R.string.Mashing_updateTVTask_Mashing_NG_Sensor_Fault_Notification_Message));
                    ((MainActivity) getActivity()).sendEmail(SettingsActivity.emailRecipient, getString(R.string.Mashing_updateTVTask_Mashing_NG_Sensor_Fault_Notification_Title), getString(R.string.Mashing_updateTVTask_Mashing_NG_Sensor_Fault_Notification_Message));

                    sensorFaultNotificationNG = true;
                } else if (!SettingsActivity.stringTemperatureSensorNG.equals(getString(R.string.Mashing_updateTVTask_Mashing_Sensor_Fault)) && !SettingsActivity.stringTemperatureSensorNG.equals("-127.00") ) {
                    sensorFaultNotificationNG = false;
                }

                if (Integer.parseInt(stringCurrentHopping) != 0) {
                    currentHopping.setText(stringCurrentHopping + getString(R.string.Cooking_Update_TV_Hopping));
                } else {
                    currentHopping.setText("");
                }

                currentHoppingTime.setText(stringCurrentHoppingTimeHours + ":" +
                        ((Integer.parseInt(stringCurrentHoppingTimeMinutes) < 10) ? "0" + stringCurrentHoppingTimeMinutes : stringCurrentHoppingTimeMinutes) + ":" +
                        ((Integer.parseInt(stringCurrentHoppingTimeSeconds) < 10) ? "0" + stringCurrentHoppingTimeSeconds : stringCurrentHoppingTimeSeconds) + " " + getString(R.string.General_hms));

                targetTemperature.setText(stringTargetTemperature + " " + getString(R.string.General_Celcius));

                currentTime.setText(stringCurrentTimeHours + ":" +
                        ((Integer.parseInt(stringCurrentTimeMinutes) < 10) ? "0" + stringCurrentTimeMinutes : stringCurrentTimeMinutes) + ":" +
                        ((Integer.parseInt(stringCurrentTimeSeconds) < 10) ? "0" + stringCurrentTimeSeconds : stringCurrentTimeSeconds) + " " + getString(R.string.General_hms));

                targetTime.setText(stringTargetCookingHours + ":" +
                        ((Integer.parseInt(stringTargetCookingMinutes) < 10) ? "0" + stringTargetCookingMinutes : stringTargetCookingMinutes) + ":00" + " " + getString(R.string.General_hms));

                if (Integer.parseInt(stringCurrentHopping) > 0) {
                    if (Recipe.dbData.getString(Recipe.dbData.getColumnIndex("HOPPING_COMMENT_" + stringCurrentHopping)).equals("")) {
                        comments.setVisibility(View.GONE);
                    } else {
                        comments.setVisibility(View.VISIBLE);
                        commentText.setText(Recipe.dbData.getString(Recipe.dbData.getColumnIndex("HOPPING_COMMENT_" + stringCurrentHopping)));
                    }
                }
            }
        });
    }
}