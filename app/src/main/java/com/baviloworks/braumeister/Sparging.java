/*
 * Copyright (C) 2016 Braumeister by BaviloWorks <BaviloWorks@gmail.com>
 *
 * This work is licensed under the Creative Commons
 * Attribution-NonCommercial-ShareAlike 4.0 International License.
 *
 * You may not use this file except in compliance with the License.
 * To view a copy of this license, visit
 *
 *      http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baviloworks.braumeister;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baviloworks.braumeister.ConnectionServices.BluetoothService;
import com.baviloworks.braumeister.ConnectionServices.WiFiService;

import java.util.Objects;

public class Sparging extends Fragment {
    private View view;

    private TextView currentTemperature;
    private TextView targetTemperature;
    private TextView currentSpargingAmount;
    private TextView targetSpargingAmount;
    public static TextView spargingStatus;
    public static CardView sparging, temperature;

    public static String stringCurrentSpargingTemperature, stringTargetSpargingTemperature, stringCurrentSpargingAmount, stringTargetSpargingAmount;
    public static String vwhNotify = "";

    public static boolean SpargingTVThreadLoop = true;
    public static boolean SpargingTVWaitLock = true;

    public static boolean spargingFinished = false;
    public static boolean spargingNotification = false;
    public static boolean vwhNotification = false;
    private boolean sensorFaultNotificationHG = false;
    private boolean sensorFaultNotificationNG = false;

    public static boolean spargingIsAlive = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.sparging, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.Start:
                start();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem start = menu.findItem(R.id.Start);

        if (MainActivity.hideSpargingItems) {
            start.setEnabled(false);
            start.getIcon().setAlpha(128);
        } else {
            start.setEnabled(true);
            start.getIcon().setAlpha(255);
        }

        Objects.requireNonNull(getActivity()).invalidateOptionsMenu();
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.sparging, container, false);

        currentTemperature = view.findViewById(R.id.Current_Temperature);
        targetTemperature = view.findViewById(R.id.Target_Temperature);
        currentSpargingAmount = view.findViewById(R.id.Current_Sparging_Amount);
        targetSpargingAmount = view.findViewById(R.id.Target_Sparging_Amount);
        spargingStatus = view.findViewById(R.id.Sparging_Status);

        temperature = view.findViewById(R.id.Temperature);
        sparging = view.findViewById(R.id.Sparging);

        new Thread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

                while (SpargingTVThreadLoop) {
                    if (spargingNotification) {
                        if ((getActivity()) != null) {
                            ((MainActivity) getActivity()).showNotification(MainActivity.spargingID, getString(R.string.Sparging_updateTVTask_Sparging_Finished_Notification_Title), getString(R.string.Sparging_updateTVTask_Sparging_Finished_Notification_Message));
                        }

                        ((MainActivity) getActivity()).sendEmail(SettingsActivity.emailRecipient, getString(R.string.Sparging_updateTVTask_Sparging_Finished_Notification_Title), getString(R.string.Sparging_updateTVTask_Sparging_Finished_Notification_Message));

                        spargingNotification = false;
                    }

                    if (vwhNotification) {
                        if ((getActivity()) != null) {
                            ((MainActivity) getActivity()).showNotification(MainActivity.spargingID, getString(R.string.Sparging_updateTVTask_VWH_Notification_Title), getString(R.string.Sparging_updateTVTask_VWH_Notification_Message));
                        }

                        ((MainActivity) getActivity()).sendEmail(SettingsActivity.emailRecipient, getString(R.string.Sparging_updateTVTask_VWH_Notification_Title), getString(R.string.Sparging_updateTVTask_VWH_Notification_Message));

                        vwhNotification = false;
                    }

                    if (!SpargingTVWaitLock) {
                        updateSparging();
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Log.e("Exception", "Could not put thread to sleep - perSecondUpdate");
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        spargingIsAlive = true;

        return view;
    }

    private void start() {
        String disconnectTitle = getResources().getString(R.string.Sparging_Start_Start_Title);
        String disconnectMessage = getResources().getString(R.string.Sparging_Start_Start_Message);

        if (MainActivity.bluetoothConnected || MainActivity.wifiConnected) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(disconnectTitle)
                    .setMessage(disconnectMessage)

                    .setPositiveButton(R.string.General_Notification_Button_Ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (MainActivity.bluetoothConnected) {
                                BluetoothService.sendMessage("S!");
                            } else {
                                WiFiService.sendMessage("S!");
                            }

                            Snackbar.make(view, R.string.Sparging_Start_Started, Snackbar.LENGTH_SHORT).show();
                        }
                    })

                    .setNegativeButton(R.string.General_Notification_Button_Cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {

                        }
                    })

                    .show();
        } else {
            Snackbar.make(view, R.string.Bluetooth_Disconnect_Not_Connected, Snackbar.LENGTH_SHORT).show();
        }
    }

    private void updateSparging() {
        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

                if (SettingsActivity.stringTemperatureSensorHG.equals("-127.00") && !sensorFaultNotificationHG) {
                    if ((getActivity()) != null) {
                        ((MainActivity) getActivity()).showNotification(MainActivity.sensorID, getString(R.string.Mashing_updateTVTask_Mashing_HG_Sensor_Fault_Notification_Title), getString(R.string.Mashing_updateTVTask_Mashing_HG_Sensor_Fault_Notification_Message));
                    }

                    ((MainActivity) getActivity()).sendEmail(SettingsActivity.emailRecipient, getString(R.string.Mashing_updateTVTask_Mashing_HG_Sensor_Fault_Notification_Title), getString(R.string.Mashing_updateTVTask_Mashing_HG_Sensor_Fault_Notification_Message));

                    sensorFaultNotificationHG = true;
                } else if (!SettingsActivity.stringTemperatureSensorHG.equals(getString(R.string.Mashing_updateTVTask_Mashing_Sensor_Fault)) && !SettingsActivity.stringTemperatureSensorHG.equals("-127.00") ) {
                    sensorFaultNotificationHG = false;
                }

                if (SettingsActivity.stringTemperatureSensorNG.equals("-127.00") && !sensorFaultNotificationNG) {
                    ((MainActivity) getActivity()).showNotification(MainActivity.sensorID, getString(R.string.Mashing_updateTVTask_Mashing_NG_Sensor_Fault_Notification_Title), getString(R.string.Mashing_updateTVTask_Mashing_NG_Sensor_Fault_Notification_Message));
                    ((MainActivity) getActivity()).sendEmail(SettingsActivity.emailRecipient, getString(R.string.Mashing_updateTVTask_Mashing_NG_Sensor_Fault_Notification_Title), getString(R.string.Mashing_updateTVTask_Mashing_NG_Sensor_Fault_Notification_Message));

                    sensorFaultNotificationNG = true;
                } else if (!SettingsActivity.stringTemperatureSensorNG.equals(getString(R.string.Mashing_updateTVTask_Mashing_Sensor_Fault)) && !SettingsActivity.stringTemperatureSensorNG.equals("-127.00") ) {
                    sensorFaultNotificationNG = false;
                }

                currentTemperature.setText(stringCurrentSpargingTemperature + " " + getString(R.string.General_Celcius));
                targetTemperature.setText(stringTargetSpargingTemperature + " " + getString(R.string.General_Celcius));

                currentSpargingAmount.setText(stringCurrentSpargingAmount + " " + getString(R.string.General_Liter));
                targetSpargingAmount.setText(stringTargetSpargingAmount + " " + getString(R.string.General_Liter));
            }
        });
    }
}