/*
 * Copyright (C) 2016 Braumeister by BaviloWorks <BaviloWorks@gmail.com>
 *
 * This work is licensed under the Creative Commons
 * Attribution-NonCommercial-ShareAlike 4.0 International License.
 *
 * You may not use this file except in compliance with the License.
 * To view a copy of this license, visit
 *
 *      http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baviloworks.braumeister;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baviloworks.braumeister.ConnectionServices.BluetoothService;
import com.baviloworks.braumeister.ConnectionServices.WiFiService;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

public class Mashing extends Fragment {
    private View view;

    public static LineChart chart;
    private LineData chartData;

    private RelativeLayout comments;
    private Button minus, plus;
    private TextView currentTemperature, targetTemperature, ngTemperature, currentTime, targetTime, commentText;
    public static TextView currentRest, mashingStatus;
    public static ImageView chartSave;
    public static CardView temperatures, times;

    public static String stringCurrentRest, stringTargetTemperature, stringCurrentTimeHours, stringCurrentTimeMinutes, stringCurrentTimeSeconds, stringTargetTimeHours, stringTargetTimeMinutes;

    public static boolean MashingTVThreadLoop = true;
    public static boolean MashingChartThreadLoop = true;
    public static boolean MashingTVWaitLock = true;
    public static boolean MashingChartWaitLock = true;
    public static boolean updateTVMashingInit = false;
    public static boolean updateTVMashing = false;
    public static boolean mashingInitFinished = false;
    public static boolean mashingFinished = false;
    public static boolean mashingNotification = false;
    public static boolean mashingInitNotification = false;
    private boolean sensorFaultNotificationHG = false;
    private boolean sensorFaultNotificationNG = false;
    public static boolean mashingIsAlive = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.mashing, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.Start:
                start();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem start = menu.findItem(R.id.Start);

        if (MainActivity.hideMashingItems) {
            start.setEnabled(false);
            start.getIcon().setAlpha(128);
        } else {
            start.setEnabled(true);
            start.getIcon().setAlpha(255);
        }

        Objects.requireNonNull(getActivity()).invalidateOptionsMenu();
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.mashing, container, false);

        currentRest = view.findViewById(R.id.Current_Rest);
        currentTemperature = view.findViewById(R.id.Current_Temperature);
        targetTemperature = view.findViewById(R.id.Target_Temperature);
        ngTemperature = view.findViewById(R.id.ng_Temperature);
        currentTime = view.findViewById(R.id.Current_Time);
        targetTime = view.findViewById(R.id.Target_Time);
        mashingStatus = view.findViewById(R.id.Mashing_Status);
        commentText = view.findViewById(R.id.Comment_Text);
        chartSave = view.findViewById(R.id.chartSave);
        temperatures = view.findViewById(R.id.Temperatures);
        times = view.findViewById(R.id.Times);
        chart = view.findViewById(R.id.chart);
        comments = view.findViewById(R.id.comments);
        minus = view.findViewById(R.id.minusMinutes);
        plus = view.findViewById(R.id.plusMinutes);

        chart.setDescription(null);
        chart.setNoDataTextDescription(getString(R.string.Mashing_Chart_No_Data));
        chart.setHighlightPerTapEnabled(true);
        chart.setTouchEnabled(true);
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        chart.setPinchZoom(true);
        chart.setHardwareAccelerationEnabled(true);

        GraphMarker mv = new GraphMarker(getActivity(), R.layout.marker);
        chart.setMarkerView(mv);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setAvoidFirstLastClipping(true);
        xAxis.setEnabled(true);

        YAxis yr = chart.getAxisRight();
        yr.setTextColor(Color.BLACK);
        yr.setAxisMinValue(40);
        yr.setAxisMaxValue(100);
        yr.setEnabled(true);

        YAxis yl = chart.getAxisLeft();
        yl.setTextColor(Color.BLACK);
        yl.setAxisMinValue(40);
        yl.setAxisMaxValue(100);
        yl.setEnabled(true);

        LineData data = new LineData();
        chart.setData(data);

        Legend l = chart.getLegend();
        l.setForm(Legend.LegendForm.LINE);
        l.setTextSize(12);
        l.setTextColor(Color.BLACK);
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);

        chart.invalidate();

        chartSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainActivity.bluetoothConnected) {
                    BluetoothService.sendMessage("-!");
                } else {
                    WiFiService.sendMessage("-!");
                }
            }
        });

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainActivity.bluetoothConnected) {
                    BluetoothService.sendMessage("+!");
                } else {
                    WiFiService.sendMessage("+!");
                }
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

                while (MashingTVThreadLoop) {
                    if (mashingInitNotification) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showNotification(MainActivity.mashingID, getString(R.string.Mashing_updateTVTask_Mashing_Init_Finished_Notification_Title), getString(R.string.Mashing_updateTVTask_Mashing_Init_Finished_Notification_Message));
                        }

                        ((MainActivity) getActivity()).sendEmail(SettingsActivity.emailRecipient, getString(R.string.Mashing_updateTVTask_Mashing_Init_Finished_Notification_Title), getString(R.string.Mashing_updateTVTask_Mashing_Init_Finished_Notification_Message));

                        mashingInitNotification = false;
                    }

                    if (mashingNotification) {
                        if (getActivity() != null) {
                            ((MainActivity) getActivity()).showNotification(MainActivity.mashingID, getString(R.string.Mashing_updateTVTask_Mashing_Finished_Notification_Title), getString(R.string.Mashing_updateTVTask_Mashing_Finished_Notification_Message));
                        }

                        ((MainActivity) getActivity()).sendEmail(SettingsActivity.emailRecipient, getString(R.string.Mashing_updateTVTask_Mashing_Finished_Notification_Title), getString(R.string.Mashing_updateTVTask_Mashing_Finished_Notification_Message));

                        mashingNotification = false;
                    }

                    if (!MashingTVWaitLock) {
                        updateMashing();
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Log.e("Exception", "Could not put thread to sleep - perSecondUpdate");
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

                while (MashingChartThreadLoop) {
                    if (!MashingChartWaitLock) {
                        addChartData();
                    }

                    try {
                        Thread.sleep(60 * 1000);
                    } catch (InterruptedException e) {
                        Log.e("Exception", "Could not put thread to sleep - perMinuteUpdate");
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        mashingIsAlive = true;

        return view;
    }

    private void start() {
        String disconnectTitle = getResources().getString(R.string.Mashing_Start_Start_Title);
        String disconnectMessage = getResources().getString(R.string.Mashing_Start_Start_Message);

        if (MainActivity.bluetoothConnected || MainActivity.wifiConnected) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(disconnectTitle)
                    .setMessage(disconnectMessage)

                    .setPositiveButton(R.string.General_Notification_Button_Ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (MainActivity.bluetoothConnected) {
                                BluetoothService.sendMessage("M!");
                            } else {
                                WiFiService.sendMessage("M!");
                            }

                            Snackbar.make(view, R.string.Mashing_Start_Started, Snackbar.LENGTH_SHORT).show();
                        }
                    })

                    .setNegativeButton(R.string.General_Notification_Button_Cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {

                        }
                    })

                    .show();
        } else {
            Snackbar.make(view, R.string.Bluetooth_Disconnect_Not_Connected, Snackbar.LENGTH_SHORT).show();
        }
    }

    private void save() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Objects.requireNonNull(getActivity()).checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setTitle(R.string.Mashing_Save_Need_Write_External_Storage_Permission_Title);
                builder.setMessage(R.string.Mashing_Save_Need_Write_External_Storage_Permission_Message);
                builder.setPositiveButton(R.string.General_Notification_Button_Ok, null);

                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        requestPermissions(new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE }, MainActivity.PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
                    }
                });

                builder.show();
            } else {
                saveToGallery();
            }
        } else {
            saveToGallery();
        }
    }

    private void saveToGallery() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Calendar.getInstance().getTime());

        if (chartData != null) {
            chart.saveToGallery(Recipe.dbData.getString(Recipe.dbData.getColumnIndex("RECIPE_NAME")) + "_" + timeStamp + ".jpg", 100);
            Snackbar.make(view, R.string.Mashing_Save_Chart_Saved, Snackbar.LENGTH_SHORT).show();
        } else {
            Snackbar.make(view, R.string.Mashing_Save_Chart_Not_Saved, Snackbar.LENGTH_SHORT).show();
        }
    }

    private LineDataSet chartData() {
        LineDataSet set = new LineDataSet(null, getString(R.string.Mashing_LineDataSet_Data_Name));
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(ColorTemplate.getHoloBlue());
        set.setLineWidth(1);
        set.setCircleRadius(0);
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setValueTextSize(0);
        set.setHighLightColor(Color.TRANSPARENT);
        set.setDrawFilled(true);

        return set;
    }

    private void addChartData() {
        chartData = chart.getData();

        if (chartData != null) {
            ILineDataSet set = chartData.getDataSetByIndex(0);

            if (set == null) {
                set = chartData();
                chartData.addDataSet(set);
            }

            Calendar now = Calendar.getInstance();
            chartData.addXValue(((now.get(Calendar.HOUR_OF_DAY) < 10)
                    ? "0" + now.get(Calendar.HOUR_OF_DAY)
                    : (now.get(Calendar.HOUR_OF_DAY))) +
                    ":" +
                    ((now.get(Calendar.MINUTE) < 10)
                            ? "0" + now.get(Calendar.MINUTE)
                            : now.get(Calendar.MINUTE)));

            Float floatCurrentTemperature = Float.valueOf(SettingsActivity.stringTemperatureSensorHG);

            chartData.addEntry(new Entry(floatCurrentTemperature, set.getEntryCount()), 0);

            chart.notifyDataSetChanged();
            chart.moveViewToX(chartData.getXValCount() - 7);
        }
    }

    private void updateMashing() {
        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

                if (updateTVMashingInit) {
                    if (SettingsActivity.stringTemperatureSensorHG.equals("-127.00") && !sensorFaultNotificationHG) {
                        currentTemperature.setText(getString(R.string.Mashing_updateTVTask_Mashing_Sensor_Fault));
                        if ((getActivity()) != null) {
                            ((MainActivity) getActivity()).showNotification(MainActivity.sensorID, getString(R.string.Mashing_updateTVTask_Mashing_HG_Sensor_Fault_Notification_Title), getString(R.string.Mashing_updateTVTask_Mashing_HG_Sensor_Fault_Notification_Message));
                        }

                        ((MainActivity) getActivity()).sendEmail(SettingsActivity.emailRecipient, getString(R.string.Mashing_updateTVTask_Mashing_HG_Sensor_Fault_Notification_Title), getString(R.string.Mashing_updateTVTask_Mashing_HG_Sensor_Fault_Notification_Message));

                        sensorFaultNotificationHG = true;
                    } else if (!SettingsActivity.stringTemperatureSensorHG.equals(getString(R.string.Mashing_updateTVTask_Mashing_Sensor_Fault)) && !SettingsActivity.stringTemperatureSensorHG.equals("-127.00") ) {
                        currentTemperature.setText(SettingsActivity.stringTemperatureSensorHG + getString(R.string.General_Celcius));

                        sensorFaultNotificationHG = false;
                    }

                    if (SettingsActivity.stringTemperatureSensorNG.equals("-127.00") && !sensorFaultNotificationNG) {
                        ngTemperature.setText(getString(R.string.Mashing_updateTVTask_Mashing_Sensor_Fault));
                        ((MainActivity) getActivity()).showNotification(MainActivity.sensorID, getString(R.string.Mashing_updateTVTask_Mashing_NG_Sensor_Fault_Notification_Title), getString(R.string.Mashing_updateTVTask_Mashing_NG_Sensor_Fault_Notification_Message));
                        ((MainActivity) getActivity()).sendEmail(SettingsActivity.emailRecipient, getString(R.string.Mashing_updateTVTask_Mashing_NG_Sensor_Fault_Notification_Title), getString(R.string.Mashing_updateTVTask_Mashing_NG_Sensor_Fault_Notification_Message));

                        sensorFaultNotificationNG = true;
                    } else if (!SettingsActivity.stringTemperatureSensorNG.equals(getString(R.string.Mashing_updateTVTask_Mashing_Sensor_Fault)) && !SettingsActivity.stringTemperatureSensorNG.equals("-127.00") ) {
                        ngTemperature.setText(SettingsActivity.stringTemperatureSensorNG + getString(R.string.General_Celcius));

                        sensorFaultNotificationNG = false;
                    }

                    targetTemperature.setText(stringTargetTemperature + getString(R.string.General_Celcius));
                }

                if (updateTVMashing) {
                    currentRest.setText(String.valueOf(Integer.parseInt(stringCurrentRest) + 1) + getString(R.string.Mashing_Update_TV_Rest));

                    currentTime.setText(stringCurrentTimeHours + ":" +
                            ((Integer.parseInt(stringCurrentTimeMinutes) < 10) ? "0" + stringCurrentTimeMinutes : stringCurrentTimeMinutes) + ":" +
                            ((Integer.parseInt(stringCurrentTimeSeconds) < 10) ? "0" + stringCurrentTimeSeconds : stringCurrentTimeSeconds) + " " + getString(R.string.General_hms));

                    targetTime.setText(stringTargetTimeHours + ":" +
                            ((Integer.parseInt(stringTargetTimeMinutes) < 10) ? "0" + stringTargetTimeMinutes : stringTargetTimeMinutes) + ":00" + " " + getString(R.string.General_hms));

                    if (Recipe.dbData.getString(Recipe.dbData.getColumnIndex("REST_COMMENT_" + String.valueOf(Integer.parseInt(stringCurrentRest) + 1))).equals("")) {
                        comments.setVisibility(View.GONE);
                    } else {
                        comments.setVisibility(View.VISIBLE);
                        commentText.setText(Recipe.dbData.getString(Recipe.dbData.getColumnIndex("REST_COMMENT_" + String.valueOf(Integer.parseInt(stringCurrentRest) + 1))));
                    }
                } else {
                    currentTime.setText("0:00:00" + " " + getString(R.string.General_Minutes));
                }
            }
        });
    }
}