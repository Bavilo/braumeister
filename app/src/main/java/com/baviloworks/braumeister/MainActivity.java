/*
 * Copyright (C) 2016 Braumeister by BaviloWorks <BaviloWorks@gmail.com>
 *
 * This work is licensed under the Creative Commons
 * Attribution-NonCommercial-ShareAlike 4.0 International License.
 *
 * You may not use this file except in compliance with the License.
 * To view a copy of this license, visit
 *
 *      http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baviloworks.braumeister;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.baviloworks.braumeister.ConnectionServices.BluetoothService;
import com.baviloworks.braumeister.ConnectionServices.WiFiService;
import com.baviloworks.braumeister.Email.GMailSender;
import com.baviloworks.braumeister.Email.HttpRequest;
import com.baviloworks.braumeister.Steg.Steg;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import static android.support.v4.content.FileProvider.getUriForFile;

public class MainActivity extends AppCompatActivity {
    final Database database = new Database(this);
    private WifiManager.WifiLock wifiLock;

    private View parentLayout;

    public static Spinner recipeLoadSelection;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    public static ViewPager mViewPager;

    public static BluetoothAdapter bluetoothAdapter;
    private IntentFilter bluetoothFilter;
    private IntentFilter dataFilter;
    private WifiManager wifiManager;

    public static final ArrayList<String> deviceList = new ArrayList<>();

    private String serialInputString;
    private String[] serialData;

    public static final String START_FLAG = "StartFlag";
    public static final String MAC_ADDRESS = "MACAddress";
    public static final String IP_ADDRESS = "IPAddress";
    public static final String NOTIFICATION = "NewData";
    public static final String DATA = "Data";

    public static final String connectionAction = "connection";
    public static final String mashingAction = "mashing";
    public static final String cookingAction = "cooking";
    public static final String hoppingAction = "cooking";
    public static final String spargingAction = "sparging";
    public static final String sensorAction = "settings";
    public static final String emailAction = "email";

    public static String file = "";

    public static final int REQUEST_ENABLE_BT = 1;
    private static final int numberOfTabs = 5;
    public static final int disconnectID = 0;
    public static final int mashingID = 1;
    public static final int cookingID = 2;
    public static final int hoppingID = 3;
    public static final int spargingID = 4;
    public static final int sensorID = 5;
    public static final int emailID = 6;

    private int pos;

    private int colorMain;
    private int colorLight;
    private int colorDark;

    public static boolean bluetoothDeviceFound = false;
    public static boolean bluetoothConnected = false;
    public static boolean wifiConnected = false;

    public static boolean hideRecipeItems = false;
    public static boolean hideMashingItems = true;
    public static boolean hideSpargingItems = true;
    public static boolean hideCookingItems = true;
    public static boolean hideSettingsItems = true;

    private boolean activityIsAlive = false;

    static final int PERMISSION_REQUEST_COARSE_LOCATION = 0;
    static final int PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.main);

        File folder = new File(Environment.getExternalStorageDirectory() + "/Braumeister");

        boolean isDirectoryCreated = folder.exists();
        if (!isDirectoryCreated) {
            folder.mkdir();
        }

        SharedPreferences sp = getSharedPreferences("prefs", Activity.MODE_PRIVATE);
        SettingsActivity.emailNotification = sp.getBoolean("emailSwitch", false);

        if (SettingsActivity.emailNotification) {
            SettingsActivity.emailRecipient = sp.getString("emailAddress", "");
        }

        parentLayout = findViewById(android.R.id.content);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (bluetoothAdapter != null) {
            bluetoothFilter = new IntentFilter();
            bluetoothFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
            bluetoothFilter.addAction(BluetoothDevice.ACTION_FOUND);
            bluetoothFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
            bluetoothFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);

            try {
                registerReceiver(mBluetoothReceiver, bluetoothFilter);
            } catch (Exception e) {
                Log.v("onCreate", "Bluetooth receiver is already registered");
            }
        } else {
            Snackbar.make(parentLayout, R.string.Main_Bluetooth_Not_Supported, Snackbar.LENGTH_LONG).show();
        }

        dataFilter = new IntentFilter();
        dataFilter.addAction(NOTIFICATION);

        try {
            registerReceiver(mDataReceiver, dataFilter);
        } catch (Exception e) {
            Log.v("onCreate", "Data receiver is already registered");
        }

        wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if (wifiManager != null && !wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }

        final Window statusBar = getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setTaskDescription(new ActivityManager.TaskDescription(null, null, ContextCompat.getColor(getApplicationContext(), R.color.Blue)));
        }

        final Toolbar toolbar = findViewById(R.id.Toolbar);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = findViewById(R.id.View_Pager);

        if (mViewPager != null) {
            mViewPager.setAdapter(mSectionsPagerAdapter);
            mViewPager.setOffscreenPageLimit(numberOfTabs - 1);
        }

        final TabLayout tabLayout = findViewById(R.id.Tabs);

        if (tabLayout != null) {
            tabLayout.setupWithViewPager(mViewPager);
            tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getApplicationContext(), R.color.LightBlue));
        }

        recipeLoadSelection = findViewById(R.id.Recipe_Selection_Spinner);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            statusBar.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.DarkBlue));
        }

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pos = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == 0) {
                    switch (pos) {
                        case 0:
                            colorMain = R.color.Blue;
                            colorLight = R.color.LightBlue;
                            colorDark = R.color.DarkBlue;

                            if (getSupportActionBar() != null) {
                                getSupportActionBar().setDisplayShowTitleEnabled(true);
                            }

                            if (Recipe.dbData.getCount() > 0) {
                                recipeLoadSelection.setVisibility(View.GONE);
                            }
                            break;
                        case 1:
                            colorMain = R.color.Green;
                            colorLight = R.color.LightGreen;
                            colorDark = R.color.DarkGreen;

                            if (getSupportActionBar() != null) {
                                getSupportActionBar().setDisplayShowTitleEnabled(false);
                            }

                            if (Recipe.dbData.getCount() > 0) {
                                recipeLoadSelection.setVisibility(View.VISIBLE);
                            }
                            break;
                        case 2:
                            colorMain = R.color.Brown;
                            colorLight = R.color.LightBrown;
                            colorDark = R.color.DarkBrown;

                            if (getSupportActionBar() != null) {
                                getSupportActionBar().setDisplayShowTitleEnabled(true);
                            }

                            if (Recipe.dbData.getCount() > 0) {
                                recipeLoadSelection.setVisibility(View.GONE);
                            }
                            break;
                        case 3:
                            colorMain = R.color.Orange;
                            colorLight = R.color.LightOrange;
                            colorDark = R.color.DarkOrange;

                            if (getSupportActionBar() != null) {
                                getSupportActionBar().setDisplayShowTitleEnabled(true);
                            }

                            if (Recipe.dbData.getCount() > 0) {
                                recipeLoadSelection.setVisibility(View.GONE);
                            }
                            break;
                        case 4:
                            colorMain = R.color.Red;
                            colorLight = R.color.LightRed;
                            colorDark = R.color.DarkRed;

                            if (getSupportActionBar() != null) {
                                getSupportActionBar().setDisplayShowTitleEnabled(true);
                            }

                            if (Recipe.dbData.getCount() > 0) {
                                recipeLoadSelection.setVisibility(View.GONE);
                            }
                            break;
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        if (getSupportActionBar() != null) {
                            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), colorMain)));
                        }

                        setTaskDescription(new ActivityManager.TaskDescription(null, null, ContextCompat.getColor(getApplicationContext(), colorMain)));

                        statusBar.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), colorDark));

                        if (tabLayout != null) {
                            tabLayout.setBackground(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), colorMain)));
                            tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getApplicationContext(), colorLight));
                        }
                    }
                }
            }
        });

        switch (Objects.requireNonNull(getIntent().getAction())) {
            case connectionAction:
                mViewPager.setCurrentItem(0);

                colorMain = R.color.Blue;
                colorLight = R.color.LightBlue;
                colorDark = R.color.DarkBlue;

                if (getSupportActionBar() != null) {
                    getSupportActionBar().setDisplayShowTitleEnabled(true);
                }

                if (Recipe.dbData != null && Recipe.dbData.getCount() > 0) {
                    recipeLoadSelection.setVisibility(View.GONE);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (getSupportActionBar() != null) {
                        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), colorMain)));
                    }

                    setTaskDescription(new ActivityManager.TaskDescription(null, null, ContextCompat.getColor(getApplicationContext(), colorMain)));

                    statusBar.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), colorDark));

                    if (tabLayout != null) {
                        tabLayout.setBackground(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), colorMain)));
                        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getApplicationContext(), colorLight));
                    }
                }
                break;
            case mashingAction:
                mViewPager.setCurrentItem(2);

                colorMain = R.color.Brown;
                colorLight = R.color.LightBrown;
                colorDark = R.color.DarkBrown;

                if (getSupportActionBar() != null) {
                    getSupportActionBar().setDisplayShowTitleEnabled(true);
                }

                if (Recipe.dbData.getCount() > 0) {
                    recipeLoadSelection.setVisibility(View.GONE);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (getSupportActionBar() != null) {
                        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), colorMain)));
                    }

                    setTaskDescription(new ActivityManager.TaskDescription(null, null, ContextCompat.getColor(getApplicationContext(), colorMain)));

                    statusBar.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), colorDark));

                    if (tabLayout != null) {
                        tabLayout.setBackground(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), colorMain)));
                        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getApplicationContext(), colorLight));
                    }
                }
                break;
            case spargingAction:
                mViewPager.setCurrentItem(3);

                colorMain = R.color.Orange;
                colorLight = R.color.LightOrange;
                colorDark = R.color.DarkOrange;

                if (getSupportActionBar() != null) {
                    getSupportActionBar().setDisplayShowTitleEnabled(true);
                }

                if (Recipe.dbData.getCount() > 0) {
                    recipeLoadSelection.setVisibility(View.GONE);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (getSupportActionBar() != null) {
                        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), colorMain)));
                    }

                    setTaskDescription(new ActivityManager.TaskDescription(null, null, ContextCompat.getColor(getApplicationContext(), colorMain)));

                    statusBar.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), colorDark));

                    if (tabLayout != null) {
                        tabLayout.setBackground(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), colorMain)));
                        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getApplicationContext(), colorLight));
                    }
                }
                break;
            case cookingAction:
                mViewPager.setCurrentItem(4);

                colorMain = R.color.Red;
                colorLight = R.color.LightRed;
                colorDark = R.color.DarkRed;

                if (getSupportActionBar() != null) {
                    getSupportActionBar().setDisplayShowTitleEnabled(true);
                }

                if (Recipe.dbData.getCount() > 0) {
                    recipeLoadSelection.setVisibility(View.GONE);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (getSupportActionBar() != null) {
                        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), colorMain)));
                    }

                    setTaskDescription(new ActivityManager.TaskDescription(null, null, ContextCompat.getColor(getApplicationContext(), colorMain)));

                    statusBar.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), colorDark));

                    if (tabLayout != null) {
                        tabLayout.setBackground(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), colorMain)));
                        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getApplicationContext(), colorLight));
                    }
                }
                break;
            case sensorAction:
                settings();
                break;
            case emailAction:
                settings();
                break;
        }

        new FileDownload().execute();

        activityIsAlive = true;
    }

    @Override
    public void onResume() {
        super.onResume();

        Mashing.MashingTVThreadLoop = true;
        Mashing.MashingChartThreadLoop = true;

        Sparging.SpargingTVThreadLoop = true;

        Cooking.CookingTVThreadLoop = true;

        try {
            registerReceiver(mBluetoothReceiver, bluetoothFilter);
        } catch (Exception e) {
            Log.v("onResume", "Bluetooth receiver is already registered");
        }

        try {
            registerReceiver(mDataReceiver, dataFilter);
        } catch (Exception e) {
            Log.v("onResume", "Data receiver is already registered");
        }

        activityIsAlive = true;
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.Main_Alert_Title_Exit)
                .setMessage(R.string.Main_Alert_Message_Exit)

                .setPositiveButton(R.string.General_Notification_Button_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish();
                    }
                })

                .setNegativeButton(R.string.General_Notification_Button_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                })

                .show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (bluetoothConnected) {
            BluetoothService.stopClient();

            bluetoothConnected = false;
        }

        if (wifiConnected) {
            WiFiService.stopClient();

            wifiConnected = false;
        }

        try {
            unregisterReceiver(mBluetoothReceiver);
        } catch (Exception e) {
            Log.v("onDestroy", "Bluetooth receiver already unregistered");
        }

        try {
            unregisterReceiver(mDataReceiver);
        } catch (Exception e) {
            Log.v("onDestroy", "Data receiver already unregistered");
        }

        Recipe.dbData.close();
        database.close();

        Mashing.MashingTVThreadLoop = false;
        Mashing.MashingChartThreadLoop = false;

        Sparging.SpargingTVThreadLoop = false;

        Cooking.CookingTVThreadLoop = false;

        activityIsAlive = false;
        Connection.connectionIsAlive = false;
        Mashing.mashingIsAlive = false;
        Sparging.spargingIsAlive = false;
        Cooking.cookingIsAlive = false;

        if (wifiLock != null && wifiLock.isHeld()) {
            wifiLock.release();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.Settings:
                settings();
                break;
            case R.id.Debugging:
                debugging();
                break;
            case R.id.Share:
                share();
                break;
            case R.id.About:
                about();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new Connection();
                case 1:
                    return new Recipe();
                case 2:
                    return new Mashing();
                case 3:
                    return new Sparging();
                case 4:
                    return new Cooking();
                default:
                    return new Connection();
            }
        }

        @Override
        public int getCount() {
            return numberOfTabs;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.Main_Tab_Connection);
                case 1:
                    return getString(R.string.Main_Tab_Recipe);
                case 2:
                    return getString(R.string.Main_Tab_Mashing);
                case 3:
                    return getString(R.string.Main_Tab_Sparging);
                case 4:
                    return getString(R.string.Main_Tab_Cooking);
            }

            return null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);

                    builder.setTitle(R.string.Main_Limited_Functionality_Title);
                    builder.setMessage(R.string.Main_Limited_Functionality_Message);
                    builder.setPositiveButton(android.R.string.ok, null);

                    builder.show();
                }

            case PERMISSION_REQUEST_COARSE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);

                    builder.setTitle(R.string.Bluetooth_Scan_Limited_Functionality_Title);
                    builder.setMessage(R.string.Bluetooth_Scan_Limited_Functionality_Message);
                    builder.setPositiveButton(android.R.string.ok, null);

                    builder.show();
                }
        }
    }

    private final BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action != null) {
                switch (action) {
                    case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                        Connection.bluetoothScanProgressbar.setVisibility(View.GONE);

                        if (!bluetoothDeviceFound) {
                            Connection.BluetoothHeader.setVisibility(View.VISIBLE);
                            Connection.bluetoothListLayout.setVisibility(View.VISIBLE);
                            Connection.bluetoothDeviceNotFound.setVisibility(View.VISIBLE);
                            Connection.bluetoothDeviceNotFound.setText(R.string.Main_Bluetooth_No_Device_Found);
                        }

                        bluetoothDeviceFound = false;
                        break;
                    case BluetoothDevice.ACTION_FOUND:
                        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                        Connection.bluetoothDeviceNotFound.setText(null);
                        Connection.bluetoothDeviceNotFound.setVisibility(View.GONE);

                        deviceList.add(device.getName() + "\n" + device.getAddress());
                        Connection.bluetoothDeviceList.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, deviceList));

                        Connection.BluetoothHeader.setVisibility(View.VISIBLE);
                        Connection.bluetoothListLayout.setVisibility(View.VISIBLE);

                        bluetoothDeviceFound = true;
                        break;
                    case BluetoothDevice.ACTION_ACL_CONNECTED:
                        Connection.ConnectionStatus.setText(R.string.Bluetooth_Layout_Connected);
                        Connection.wifiDeviceList.setEnabled(false);
                        Connection.wifiDeviceList.setAlpha(0.5f);
                        Snackbar.make(parentLayout, R.string.Wifi_Connect_Connected, Snackbar.LENGTH_SHORT).show();

                        if (Connection.disconnectProgressBluetooth != null && Connection.disconnectProgressBluetooth.isShowing()) {
                            Connection.disconnectProgressBluetooth.dismiss();
                        }

                        if (Connection.connectProgressBluetooth != null && Connection.connectProgressBluetooth.isShowing()) {
                            Connection.connectProgressBluetooth.dismiss();
                        }

                        mViewPager.setCurrentItem(1);

                        bluetoothConnected = true;
                        break;
                    case BluetoothDevice.ACTION_ACL_DISCONNECTED:
                        if (Recipe.dataSent) {
                            recipeLoadSelection.setEnabled(false);

                            Recipe.addRecipe.setVisibility(View.GONE);
                            Recipe.deleteRecipe.setVisibility(View.GONE);
                        } else {
                            recipeLoadSelection.setEnabled(true);

                            Recipe.addRecipe.setVisibility(View.VISIBLE);
                            Recipe.deleteRecipe.setVisibility(View.VISIBLE);
                        }

                        hideRecipeItems = true;
                        hideMashingItems = true;
                        hideSpargingItems = true;
                        hideCookingItems = true;
                        hideSettingsItems = true;

                        bluetoothConnected = false;

                        Connection.ConnectionStatus.setText(R.string.Bluetooth_Layout_Not_Connected);
                        Connection.wifiDeviceList.setEnabled(true);
                        Connection.wifiDeviceList.setAlpha(1.0f);

                        if (Connection.disconnectPressed) {
                            Snackbar.make(parentLayout, R.string.Wifi_Connect_Disconnected, Snackbar.LENGTH_SHORT).show();

                            Connection.disconnectPressed = false;
                        }

                        if (Connection.disconnectProgressBluetooth != null && Connection.disconnectProgressBluetooth.isShowing()) {
                            Connection.disconnectProgressBluetooth.dismiss();
                        }

                        if (Connection.connectProgressBluetooth != null && Connection.connectProgressBluetooth.isShowing()) {
                            Connection.connectProgressBluetooth.dismiss();
                        }

                        Mashing.mashingStatus.setText(R.string.Mashing_Layout_Not_Started);
                        Mashing.currentRest.setText("");

                        Sparging.spargingStatus.setText(R.string.Sparging_Layout_Not_Started);

                        Cooking.cookingStatus.setText(R.string.Mashing_Layout_Not_Started);
                        Cooking.currentHopping.setText("");

                        SettingsActivity.stringTemperatureSensorHG = "";
                        SettingsActivity.stringTemperatureSensorNG = "";
                        SettingsActivity.stringLgHysteresis = "";
                        SettingsActivity.stringLbHysteresis = "";
                        SettingsActivity.stringLgPumpTime = "";
                        SettingsActivity.stringLbPumpTime = "";

                        Mashing.temperatures.setVisibility(View.GONE);
                        Mashing.times.setVisibility(View.GONE);
                        Mashing.chart.setVisibility(View.GONE);
                        Mashing.chartSave.setVisibility(View.GONE);

                        Sparging.temperature.setVisibility(View.GONE);
                        Sparging.sparging.setVisibility(View.GONE);
                        Cooking.temperatures.setVisibility(View.GONE);
                        Cooking.times.setVisibility(View.GONE);
                        Cooking.hoppings.setVisibility(View.GONE);

                        Mashing.MashingTVWaitLock = true;
                        Mashing.MashingChartWaitLock = true;

                        Sparging.SpargingTVWaitLock = true;

                        Cooking.CookingTVWaitLock = true;
                        break;
                }
            }
        }
    };

    private final BroadcastReceiver mDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String newSerialData = bundle.getString(DATA);

                if (newSerialData != null && activityIsAlive &&
                        Mashing.mashingIsAlive && Sparging.spargingIsAlive && Cooking.cookingIsAlive) {
                    switch (newSerialData) {
                        case "ConnectWiFi":
                            Connection.ConnectionStatus.setText(R.string.Wifi_Layout_Connected);
                            Connection.bluetoothDeviceList.setEnabled(false);
                            Connection.bluetoothDeviceList.setAlpha(0.5f);
                            Snackbar.make(parentLayout, R.string.Wifi_Connect_Connected, Snackbar.LENGTH_SHORT).show();

                            if (Connection.disconnectProgressWiFi != null && Connection.disconnectProgressWiFi.isShowing()) {
                                Connection.disconnectProgressWiFi.dismiss();
                            }

                            if (Connection.connectProgressWiFi != null && Connection.connectProgressWiFi.isShowing()) {
                                Connection.connectProgressWiFi.dismiss();
                            }

                            mViewPager.setCurrentItem(1);

                            wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                            if (wifiManager != null) {
                                wifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL, "MyWifiLock");
                            }

                            if (wifiLock != null && !wifiLock.isHeld()) {
                                wifiLock.acquire();
                            }

                            wifiConnected = true;
                            break;
                        case "DisconnectWiFi":
                            if (Recipe.dataSent) {
                                recipeLoadSelection.setEnabled(false);

                                Recipe.addRecipe.setVisibility(View.GONE);
                                Recipe.deleteRecipe.setVisibility(View.GONE);
                            } else {
                                recipeLoadSelection.setEnabled(true);

                                Recipe.addRecipe.setVisibility(View.VISIBLE);
                                Recipe.deleteRecipe.setVisibility(View.VISIBLE);
                            }

                            hideRecipeItems = true;
                            hideMashingItems = true;
                            hideSpargingItems = true;
                            hideCookingItems = true;
                            hideSettingsItems = true;

                            wifiConnected = false;

                            Connection.ConnectionStatus.setText(R.string.Wifi_Layout_Not_Connected);
                            Connection.bluetoothDeviceList.setEnabled(true);
                            Connection.bluetoothDeviceList.setAlpha(1.0f);

                            if (Connection.disconnectPressed) {
                                Snackbar.make(parentLayout, R.string.Wifi_Connect_Disconnected, Snackbar.LENGTH_SHORT).show();

                                Connection.disconnectPressed = false;
                            }

                            if (Connection.disconnectProgressWiFi != null && Connection.disconnectProgressWiFi.isShowing()) {
                                Connection.disconnectProgressWiFi.dismiss();
                            }

                            if (Connection.connectProgressWiFi != null && Connection.connectProgressWiFi.isShowing()) {
                                Connection.connectProgressWiFi.dismiss();
                            }

                            Mashing.mashingStatus.setText(R.string.Mashing_Layout_Not_Started);
                            Mashing.currentRest.setText("");

                            Sparging.spargingStatus.setText(R.string.Sparging_Layout_Not_Started);

                            Cooking.cookingStatus.setText(R.string.Mashing_Layout_Not_Started);
                            Cooking.currentHopping.setText("");

                            SettingsActivity.stringTemperatureSensorHG = "";
                            SettingsActivity.stringTemperatureSensorNG = "";
                            SettingsActivity.stringLgHysteresis = "";
                            SettingsActivity.stringLbHysteresis = "";
                            SettingsActivity.stringLgPumpTime = "";
                            SettingsActivity.stringLbPumpTime = "";

                            Mashing.temperatures.setVisibility(View.GONE);
                            Mashing.times.setVisibility(View.GONE);
                            Mashing.chart.setVisibility(View.GONE);
                            Mashing.chartSave.setVisibility(View.GONE);

                            Sparging.temperature.setVisibility(View.GONE);
                            Sparging.sparging.setVisibility(View.GONE);
                            Cooking.temperatures.setVisibility(View.GONE);
                            Cooking.times.setVisibility(View.GONE);
                            Cooking.hoppings.setVisibility(View.GONE);

                            Mashing.MashingTVWaitLock = true;
                            Mashing.MashingChartWaitLock = true;

                            Sparging.SpargingTVWaitLock = true;

                            Cooking.CookingTVWaitLock = true;
                            break;
                        default:
                            int endOfLineIndex = newSerialData.indexOf("!");

                            if (endOfLineIndex > 0) {
                                switch (newSerialData.charAt(0)) {
                                    case 'I':
                                        serialInputString = newSerialData.substring(1, endOfLineIndex);
                                        serialData = serialInputString.split(",");

                                        SettingsActivity.stringTemperatureSensorHG = serialData[0];
                                        SettingsActivity.stringTemperatureSensorNG = serialData[1];
                                        SettingsActivity.stringLgHysteresis = serialData[2];
                                        SettingsActivity.stringLbHysteresis = serialData[3];
                                        SettingsActivity.stringLgPumpTime = serialData[4];
                                        SettingsActivity.stringLbPumpTime = serialData[5];

                                        Mashing.stringTargetTemperature = serialData[6];

                                        Mashing.MashingTVWaitLock = false;
                                        Mashing.updateTVMashingInit = true;

                                        Mashing.mashingStatus.setText(R.string.Mashing_Mashing_Init_Started);

                                        Mashing.temperatures.setVisibility(View.VISIBLE);

                                        Recipe.dataSent = true;

                                        hideRecipeItems = true;
                                        hideMashingItems = true;
                                        hideSpargingItems = true;
                                        hideCookingItems = true;
                                        hideSettingsItems = false;

                                        recipeLoadSelection.setEnabled(false);

                                        Recipe.addRecipe.setVisibility(View.GONE);
                                        Recipe.deleteRecipe.setVisibility(View.GONE);
                                        break;
                                    case '/':
                                        serialInputString = newSerialData.substring(1, endOfLineIndex);
                                        serialData = serialInputString.split(",");

                                        SettingsActivity.stringTemperatureSensorHG = serialData[0];
                                        SettingsActivity.stringTemperatureSensorNG = serialData[1];
                                        SettingsActivity.stringLgHysteresis = serialData[2];
                                        SettingsActivity.stringLbHysteresis = serialData[3];
                                        SettingsActivity.stringLgPumpTime = serialData[4];
                                        SettingsActivity.stringLbPumpTime = serialData[5];

                                        Mashing.stringTargetTemperature = serialData[6];

                                        Mashing.MashingTVWaitLock = false;
                                        Mashing.updateTVMashingInit = true;

                                        Mashing.mashingStatus.setText(R.string.Mashing_Mashing_Init_Finished);

                                        Mashing.temperatures.setVisibility(View.VISIBLE);

                                        if (!Mashing.mashingInitFinished) {
                                            Mashing.mashingInitNotification = true;

                                            Mashing.mashingInitFinished = true;
                                        }

                                        Recipe.dataSent = true;

                                        hideRecipeItems = true;
                                        hideMashingItems = false;
                                        hideSpargingItems = true;
                                        hideCookingItems = true;
                                        hideSettingsItems = false;

                                        recipeLoadSelection.setEnabled(false);

                                        Recipe.addRecipe.setVisibility(View.GONE);
                                        Recipe.deleteRecipe.setVisibility(View.GONE);
                                        break;
                                    case 'M':
                                        serialInputString = newSerialData.substring(1, endOfLineIndex);
                                        serialData = serialInputString.split(",");

                                        Mashing.stringCurrentRest = serialData[0];

                                        SettingsActivity.stringTemperatureSensorHG = serialData[1];
                                        SettingsActivity.stringTemperatureSensorNG = serialData[2];
                                        SettingsActivity.stringLgHysteresis = serialData[3];
                                        SettingsActivity.stringLbHysteresis = serialData[4];
                                        SettingsActivity.stringLgPumpTime = serialData[5];
                                        SettingsActivity.stringLbPumpTime = serialData[6];

                                        Mashing.stringTargetTemperature = serialData[7];
                                        Mashing.stringCurrentTimeHours = serialData[8];
                                        Mashing.stringCurrentTimeMinutes = serialData[9];
                                        Mashing.stringCurrentTimeSeconds = serialData[10];
                                        Mashing.stringTargetTimeHours = serialData[11];
                                        Mashing.stringTargetTimeMinutes = serialData[12];

                                        Mashing.MashingTVWaitLock = false;
                                        Mashing.MashingChartWaitLock = false;
                                        Mashing.updateTVMashingInit = true;
                                        Mashing.updateTVMashing = true;

                                        Mashing.mashingInitFinished = true;

                                        Mashing.mashingStatus.setText(R.string.Mashing_Layout_Started);

                                        Mashing.temperatures.setVisibility(View.VISIBLE);
                                        Mashing.times.setVisibility(View.VISIBLE);
                                        Mashing.chart.setVisibility(View.VISIBLE);
                                        Mashing.chartSave.setVisibility(View.VISIBLE);

                                        Recipe.dataSent = true;

                                        hideRecipeItems = true;
                                        hideMashingItems = true;
                                        hideSpargingItems = true;
                                        hideCookingItems = true;
                                        hideSettingsItems = false;

                                        recipeLoadSelection.setEnabled(false);

                                        Recipe.addRecipe.setVisibility(View.GONE);
                                        Recipe.deleteRecipe.setVisibility(View.GONE);
                                        break;
                                    case '#':
                                        serialInputString = newSerialData.substring(1, endOfLineIndex);
                                        serialData = serialInputString.split(",");

                                        SettingsActivity.stringTemperatureSensorHG = serialData[0];
                                        SettingsActivity.stringTemperatureSensorNG = serialData[1];
                                        SettingsActivity.stringLgHysteresis = serialData[2];
                                        SettingsActivity.stringLbHysteresis = serialData[3];
                                        SettingsActivity.stringLgPumpTime = serialData[4];
                                        SettingsActivity.stringLbPumpTime = serialData[5];

                                        Mashing.MashingTVWaitLock = true;
                                        Mashing.MashingChartWaitLock = true;
                                        Mashing.updateTVMashingInit = false;
                                        Mashing.updateTVMashing = false;

                                        Mashing.mashingInitFinished = true;

                                        Mashing.mashingStatus.setText(R.string.Mashing_Layout_Finished);

                                        if (!Mashing.mashingFinished) {
                                            Mashing.mashingNotification = true;

                                            Mashing.mashingFinished = true;
                                        }

                                        Recipe.dataSent = true;

                                        hideRecipeItems = true;
                                        hideMashingItems = true;
                                        hideSpargingItems = false;
                                        hideCookingItems = true;
                                        hideSettingsItems = false;

                                        recipeLoadSelection.setEnabled(false);

                                        Recipe.addRecipe.setVisibility(View.GONE);
                                        Recipe.deleteRecipe.setVisibility(View.GONE);
                                        break;
                                    case 'S':
                                        serialInputString = newSerialData.substring(1, endOfLineIndex);
                                        serialData = serialInputString.split(",");

                                        Sparging.stringCurrentSpargingAmount = serialData[0];
                                        Sparging.stringTargetSpargingAmount = serialData[1];

                                        SettingsActivity.stringTemperatureSensorHG = serialData[2];
                                        SettingsActivity.stringTemperatureSensorNG = serialData[3];

                                        Sparging.stringCurrentSpargingTemperature = serialData[3];
                                        Sparging.stringTargetSpargingTemperature = serialData[4];

                                        SettingsActivity.stringLgHysteresis = serialData[5];
                                        SettingsActivity.stringLbHysteresis = serialData[6];
                                        SettingsActivity.stringLgPumpTime = serialData[7];
                                        SettingsActivity.stringLbPumpTime = serialData[8];

                                        Sparging.vwhNotify = serialData[9];

                                        if (Sparging.vwhNotify.equals("true")) {
                                            Sparging.vwhNotification = true;

                                            Sparging.vwhNotify = "false";
                                        }

                                        Mashing.MashingTVThreadLoop = false;
                                        Mashing.MashingChartThreadLoop = false;

                                        Sparging.SpargingTVWaitLock = false;

                                        Mashing.mashingInitFinished = true;
                                        Mashing.mashingFinished = true;

                                        Mashing.mashingStatus.setText(R.string.Mashing_Layout_Finished);
                                        Sparging.spargingStatus.setText(R.string.Sparging_Layout_Started);

                                        Sparging.temperature.setVisibility(View.VISIBLE);
                                        Sparging.sparging.setVisibility(View.VISIBLE);

                                        Recipe.dataSent = true;

                                        hideRecipeItems = true;
                                        hideMashingItems = true;
                                        hideSpargingItems = true;
                                        hideCookingItems = false;
                                        hideSettingsItems = false;

                                        recipeLoadSelection.setEnabled(false);

                                        Recipe.addRecipe.setVisibility(View.GONE);
                                        Recipe.deleteRecipe.setVisibility(View.GONE);
                                        break;
                                    case '%':
                                        serialInputString = newSerialData.substring(1, endOfLineIndex);
                                        serialData = serialInputString.split(",");

                                        SettingsActivity.stringTemperatureSensorHG = serialData[0];
                                        SettingsActivity.stringTemperatureSensorNG = serialData[1];
                                        SettingsActivity.stringLgHysteresis = serialData[2];
                                        SettingsActivity.stringLbHysteresis = serialData[3];
                                        SettingsActivity.stringLgPumpTime = serialData[4];
                                        SettingsActivity.stringLbPumpTime = serialData[5];

                                        Mashing.MashingTVThreadLoop = false;
                                        Mashing.MashingChartThreadLoop = false;

                                        Sparging.SpargingTVWaitLock = true;

                                        Mashing.mashingInitFinished = true;
                                        Mashing.mashingFinished = true;

                                        Mashing.mashingStatus.setText(R.string.Mashing_Layout_Finished);
                                        Sparging.spargingStatus.setText(R.string.Sparging_Layout_Finished);

                                        if (!Sparging.spargingFinished) {
                                            Sparging.spargingNotification = true;

                                            Sparging.spargingFinished = true;
                                        }

                                        Recipe.dataSent = true;

                                        hideRecipeItems = true;
                                        hideMashingItems = true;
                                        hideSpargingItems = true;
                                        hideCookingItems = false;
                                        hideSettingsItems = false;

                                        recipeLoadSelection.setEnabled(false);

                                        Recipe.addRecipe.setVisibility(View.GONE);
                                        Recipe.deleteRecipe.setVisibility(View.GONE);
                                        break;
                                    case 'N':
                                        serialInputString = newSerialData.substring(1, endOfLineIndex);
                                        serialData = serialInputString.split(",");

                                        Cooking.hoppingNotify = serialData[0];
                                        Cooking.stringCurrentHopping = serialData[1];

                                        Sparging.stringCurrentSpargingAmount = serialData[2];
                                        Sparging.stringTargetSpargingAmount = serialData[3];

                                        SettingsActivity.stringTemperatureSensorHG = serialData[4];
                                        SettingsActivity.stringTemperatureSensorNG = serialData[5];

                                        Sparging.stringCurrentSpargingTemperature = serialData[5];
                                        Sparging.stringTargetSpargingTemperature = serialData[6];

                                        SettingsActivity.stringLgHysteresis = serialData[7];
                                        SettingsActivity.stringLbHysteresis = serialData[8];
                                        SettingsActivity.stringLgPumpTime = serialData[9];
                                        SettingsActivity.stringLbPumpTime = serialData[10];

                                        Cooking.stringTargetTemperature = serialData[11];
                                        Cooking.stringCurrentTimeHours = serialData[12];
                                        Cooking.stringCurrentTimeMinutes = serialData[13];
                                        Cooking.stringCurrentTimeSeconds = serialData[14];
                                        Cooking.stringTargetCookingHours = serialData[15];
                                        Cooking.stringTargetCookingMinutes = serialData[16];
                                        Cooking.stringCurrentHoppingTimeHours = serialData[17];
                                        Cooking.stringCurrentHoppingTimeMinutes = serialData[18];
                                        Cooking.stringCurrentHoppingTimeSeconds = serialData[19];

                                        Sparging.vwhNotify = serialData[20];

                                        if (Cooking.hoppingNotify.equals("true")) {
                                            Cooking.hoppingNotification = true;

                                            Cooking.hoppingNotify = "false";
                                        }

                                        if (Sparging.vwhNotify.equals("true")) {
                                            Sparging.vwhNotification = true;

                                            Sparging.vwhNotify = "false";
                                        }

                                        Mashing.MashingTVThreadLoop = false;
                                        Mashing.MashingChartThreadLoop = false;

                                        Sparging.SpargingTVWaitLock = false;
                                        Cooking.CookingTVWaitLock = false;

                                        Mashing.mashingInitFinished = true;
                                        Mashing.mashingFinished = true;

                                        Mashing.mashingStatus.setText(R.string.Mashing_Layout_Finished);
                                        Sparging.spargingStatus.setText(R.string.Sparging_Layout_Started);
                                        Cooking.cookingStatus.setText(R.string.Mashing_Layout_Started);

                                        Sparging.temperature.setVisibility(View.VISIBLE);
                                        Sparging.sparging.setVisibility(View.VISIBLE);
                                        Cooking.temperatures.setVisibility(View.VISIBLE);
                                        Cooking.times.setVisibility(View.VISIBLE);
                                        Cooking.hoppings.setVisibility(View.VISIBLE);

                                        Recipe.dataSent = true;

                                        hideRecipeItems = true;
                                        hideMashingItems = true;
                                        hideSpargingItems = true;
                                        hideCookingItems = true;
                                        hideSettingsItems = false;

                                        recipeLoadSelection.setEnabled(false);

                                        Recipe.addRecipe.setVisibility(View.GONE);
                                        Recipe.deleteRecipe.setVisibility(View.GONE);
                                        break;
                                    case 'K':
                                        serialInputString = newSerialData.substring(1, endOfLineIndex);
                                        serialData = serialInputString.split(",");

                                        Cooking.hoppingNotify = serialData[0];
                                        Cooking.stringCurrentHopping = serialData[1];

                                        SettingsActivity.stringTemperatureSensorHG = serialData[2];
                                        SettingsActivity.stringTemperatureSensorNG = serialData[3];
                                        SettingsActivity.stringLgHysteresis = serialData[4];
                                        SettingsActivity.stringLbHysteresis = serialData[5];
                                        SettingsActivity.stringLgPumpTime = serialData[6];
                                        SettingsActivity.stringLbPumpTime = serialData[7];

                                        Cooking.stringTargetTemperature = serialData[8];
                                        Cooking.stringCurrentTimeHours = serialData[9];
                                        Cooking.stringCurrentTimeMinutes = serialData[10];
                                        Cooking.stringCurrentTimeSeconds = serialData[11];
                                        Cooking.stringTargetCookingHours = serialData[12];
                                        Cooking.stringTargetCookingMinutes = serialData[13];
                                        Cooking.stringCurrentHoppingTimeHours = serialData[14];
                                        Cooking.stringCurrentHoppingTimeMinutes = serialData[15];
                                        Cooking.stringCurrentHoppingTimeSeconds = serialData[16];

                                        if (Cooking.hoppingNotify.equals("true")) {
                                            Cooking.hoppingNotification = true;

                                            Cooking.hoppingNotify = "false";
                                        }

                                        Mashing.MashingTVThreadLoop = false;
                                        Mashing.MashingChartThreadLoop = false;
                                        Sparging.SpargingTVThreadLoop = false;

                                        Cooking.CookingTVWaitLock = false;

                                        Mashing.mashingInitFinished = true;
                                        Mashing.mashingFinished = true;
                                        Sparging.spargingFinished = true;

                                        Mashing.mashingStatus.setText(R.string.Mashing_Layout_Finished);
                                        Sparging.spargingStatus.setText(R.string.Sparging_Layout_Finished);
                                        Cooking.cookingStatus.setText(R.string.Mashing_Layout_Started);

                                        Cooking.temperatures.setVisibility(View.VISIBLE);
                                        Cooking.times.setVisibility(View.VISIBLE);
                                        Cooking.hoppings.setVisibility(View.VISIBLE);

                                        Recipe.dataSent = true;

                                        hideRecipeItems = true;
                                        hideMashingItems = true;
                                        hideSpargingItems = true;
                                        hideCookingItems = true;
                                        hideSettingsItems = false;

                                        recipeLoadSelection.setEnabled(false);

                                        Recipe.addRecipe.setVisibility(View.GONE);
                                        Recipe.deleteRecipe.setVisibility(View.GONE);
                                        break;
                                    case '&':
                                        serialInputString = newSerialData.substring(1, endOfLineIndex);
                                        serialData = serialInputString.split(",");

                                        SettingsActivity.stringTemperatureSensorHG = serialData[0];
                                        SettingsActivity.stringTemperatureSensorNG = serialData[1];
                                        SettingsActivity.stringLgHysteresis = serialData[2];
                                        SettingsActivity.stringLbHysteresis = serialData[3];
                                        SettingsActivity.stringLgPumpTime = serialData[4];
                                        SettingsActivity.stringLbPumpTime = serialData[5];

                                        Mashing.MashingTVThreadLoop = false;
                                        Mashing.MashingChartThreadLoop = false;
                                        Sparging.SpargingTVThreadLoop = false;

                                        Cooking.CookingTVWaitLock = true;

                                        Mashing.mashingInitFinished = true;
                                        Mashing.mashingFinished = true;
                                        Sparging.spargingFinished = true;

                                        Mashing.mashingStatus.setText(R.string.Mashing_Layout_Finished);
                                        Sparging.spargingStatus.setText(R.string.Sparging_Layout_Finished);
                                        Cooking.cookingStatus.setText(R.string.Cooking_Layout_Finished);

                                        if (!Cooking.cookingFinished) {
                                            Cooking.cookingNotification = true;

                                            Cooking.cookingFinished = true;
                                        }

                                        Recipe.dataSent = true;

                                        hideRecipeItems = true;
                                        hideMashingItems = true;
                                        hideSpargingItems = true;
                                        hideCookingItems = true;
                                        hideSettingsItems = false;

                                        if (Recipe.dataSent) {
                                            recipeLoadSelection.setEnabled(false);

                                            Recipe.addRecipe.setVisibility(View.GONE);
                                            Recipe.deleteRecipe.setVisibility(View.GONE);
                                        } else {
                                            recipeLoadSelection.setEnabled(true);

                                            Recipe.addRecipe.setVisibility(View.VISIBLE);
                                            Recipe.deleteRecipe.setVisibility(View.VISIBLE);
                                        }

                                        break;
                                    case 'W':
                                        serialInputString = newSerialData.substring(1, endOfLineIndex);
                                        serialData = serialInputString.split(",");

                                        SettingsActivity.stringTemperatureSensorHG = serialData[0];
                                        SettingsActivity.stringTemperatureSensorNG = serialData[1];
                                        SettingsActivity.stringLgHysteresis = serialData[2];
                                        SettingsActivity.stringLbHysteresis = serialData[3];
                                        SettingsActivity.stringLgPumpTime = serialData[4];
                                        SettingsActivity.stringLbPumpTime = serialData[5];

                                        hideRecipeItems = false;
                                        hideMashingItems = true;
                                        hideSpargingItems = true;
                                        hideCookingItems = true;
                                        hideSettingsItems = false;

                                        if (Recipe.dataSent) {
                                            recipeLoadSelection.setEnabled(false);

                                            Recipe.addRecipe.setVisibility(View.GONE);
                                            Recipe.deleteRecipe.setVisibility(View.GONE);
                                        } else {
                                            recipeLoadSelection.setEnabled(true);

                                            Recipe.addRecipe.setVisibility(View.VISIBLE);
                                            Recipe.deleteRecipe.setVisibility(View.VISIBLE);
                                        }
                                        break;
                                }
                            }
                            break;
                    }
                }
            }
        }
    };

    public void showNotification(int ID, String title, String message) {
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_notification_logo);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent resultIntent = new Intent(this, MainActivity.class);

        switch (ID) {
            case 0:
                resultIntent.setAction(connectionAction);
                break;
            case 1:
                resultIntent.setAction(mashingAction);
                break;
            case 2:
                resultIntent.setAction(cookingAction);
                break;
            case 3:
                resultIntent.setAction(hoppingAction);
                break;
            case 4:
                resultIntent.setAction(spargingAction);
                break;
            case 5:
                resultIntent.setAction(sensorAction);
                break;
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "M_CH_ID")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(getApplicationContext(), R.color.NotificationBackground))
                .setSmallIcon(R.drawable.ic_announcement_white_24dp)
                .setLargeIcon(largeIcon)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setLights(ContextCompat.getColor(getApplicationContext(), R.color.Blue), 300, 1000)
                .setSound(soundUri)
                .setContentIntent(PendingIntent.getActivity(this, 0, resultIntent, 0));

        NotificationManager notifyMgr = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        if (notifyMgr != null) {
            notifyMgr.notify(ID, builder.build());
        }
    }

    public void sendEmail(final String recipient, final String subject, final String body) {
        new Thread(new Runnable() {
            public void run() {
                if (SettingsActivity.emailNotification && !recipient.equals("")) {
                    if (!file.equals("")) {
                        try {
                            GMailSender sender = new GMailSender();

                            sender.sendMail(subject, body, recipient);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        showNotification(emailID, getString(R.string.Main_Email_No_Password_Title), getString(R.string.Main_Email_No_Password_Message));
                    }
                }
            }
        }).start();
    }

    private void about() {
        LayoutInflater inflater = getLayoutInflater();
        View aboutView = inflater.inflate(R.layout.about, null);

        AlertDialog.Builder aboutDialog = new AlertDialog.Builder(this);
        aboutDialog.setView(aboutView);

        Spanned s;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            s = Html.fromHtml(getString(R.string.Main_Menu_Info_Message), Html.FROM_HTML_MODE_LEGACY);
        } else {
            //noinspection deprecation
            s = Html.fromHtml(getString(R.string.Main_Menu_Info_Message));
        }

        TextView aboutText = aboutView.findViewById(R.id.aboutText);
        aboutText.setText(s);
        aboutText.setMovementMethod(LinkMovementMethod.getInstance());

        aboutDialog.setTitle(R.string.Main_Menu_Info_Title);
        aboutDialog.setPositiveButton(R.string.General_Notification_Button_Ok, null);
        aboutDialog.show();
    }

    private void settings() {
        Intent myIntent = new Intent(MainActivity.this, SettingsActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    private void debugging() {
        Intent myIntent = new Intent(MainActivity.this, DebuggingActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    private void share() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setTitle(R.string.Main_Share_Need_Write_External_Storage_Permission_Title);
                builder.setMessage(R.string.Main_Share_Need_Write_External_Storage_Permission_Message);
                builder.setPositiveButton(R.string.General_Notification_Button_Ok, null);

                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MainActivity.PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
                    }
                });

                builder.show();
            } else {
                shareRecipeFile();
            }
        } else {
            shareRecipeFile();
        }
    }

    private void shareRecipeFile() {
        if (Recipe.dbData.getCount() > 0) {
            Recipe.dbData = (Cursor) Recipe.cursorAdapter.getItem(recipeLoadSelection.getSelectedItemPosition());

            String recipeNameShare = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("RECIPE_NAME"));
            final String recipeNameShareFile = recipeNameShare.replace(" ", "_");

            String minRequirementMashingTemp = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("MASHING_TEMPERATURE"));
            String minRequirementRestTempOne = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("REST_TEMPERATURE_1"));
            String minRequirementRestTimeOne = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("REST_TIME_1"));
            String minRequirementRestTempTwo = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("REST_TEMPERATURE_2"));
            String minRequirementRestTimeTwo = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("REST_TIME_2"));
            String minRequirementSpargeAmount = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("SPARGE_AMOUNT"));
            String minRequirementCookingDuration = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("TOTAL_COOKING_DURATION"));
            String minRequirementHopTimeOne = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("COOKING_DURATION_1"));

            if (minRequirementMashingTemp != null && minRequirementRestTempOne != null &&
                    minRequirementRestTimeOne != null && minRequirementRestTempTwo != null &&
                    minRequirementRestTimeTwo != null && minRequirementSpargeAmount != null &&
                    minRequirementCookingDuration != null && minRequirementHopTimeOne != null) {
                final String finalRecipeNameShare = recipeNameShare;

                new AlertDialog.Builder(this)
                        .setTitle(getResources().getString(R.string.Main_Share_Share_Recipe_Title))
                        .setMessage(String.format(getResources().getString(R.string.Main_Share_Share_Recipe_Message), recipeNameShare))

                        .setPositiveButton(R.string.General_Notification_Button_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String cookingDurationShare = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("TOTAL_COOKING_DURATION"));
                                String mashingTempShare = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("MASHING_TEMPERATURE"));
                                String spargingAmountShare = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("SPARGE_AMOUNT"));
                                String isVWHShare = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("IS_VWH"));

                                Integer restAmountShare = Integer.valueOf(Recipe.dbData.getString(Recipe.dbData.getColumnIndex("AMOUNT_OF_RESTS")));
                                Integer hopAmountShare = Integer.valueOf(Recipe.dbData.getString(Recipe.dbData.getColumnIndex("AMOUNT_OF_HOPPINGS")));

                                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                                DocumentBuilder dBuilder;

                                try {
                                    dBuilder = dbFactory.newDocumentBuilder();
                                    Document doc = dBuilder.newDocument();

                                    Element root = doc.createElement("xsud");
                                    Element sud = doc.createElement("Sud");
                                    Element sudName = doc.createElement("Sudname");
                                    Element cookingDuration = doc.createElement("KochdauerNachBitterhopfung");
                                    Element mashingTemp = doc.createElement("EinmaischenTemp");
                                    Element spargingAmount = doc.createElement("erg_WNachguss");
                                    Element isVWH = doc.createElement("VWH");
                                    Element rests = doc.createElement("Rasten");
                                    Element hops = doc.createElement("Hopfengaben");

                                    doc.appendChild(root);

                                    root.appendChild(sud);

                                    sud.appendChild(sudName);
                                    sudName.appendChild(doc.createTextNode(finalRecipeNameShare));

                                    sud.appendChild(cookingDuration);
                                    cookingDuration.appendChild(doc.createTextNode(cookingDurationShare));

                                    sud.appendChild(mashingTemp);
                                    mashingTemp.appendChild(doc.createTextNode(mashingTempShare));

                                    sud.appendChild(spargingAmount);
                                    spargingAmount.appendChild(doc.createTextNode(spargingAmountShare));

                                    sud.appendChild(isVWH);
                                    isVWH.appendChild(doc.createTextNode(isVWHShare));

                                    sud.appendChild(rests);

                                    for (int i = 0; i < restAmountShare; i++) {
                                        rests.appendChild(doc.createElement("Rast_" + (i + 1)));

                                        NodeList list = rests.getChildNodes();
                                        Element currentRest = (Element) list.item(i);

                                        Element restName = doc.createElement("RastName");
                                        Element restTemp = doc.createElement("RastTemp");
                                        Element restDuration = doc.createElement("RastDauer");

                                        Integer currentRestSelectionShare = Integer.valueOf(Recipe.dbData.getString(Recipe.dbData.getColumnIndex("REST_SELECTION_" + (i + 1))));
                                        String currentRestTempShare = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("REST_TEMPERATURE_" + (i + 1)));
                                        String currentRestTimeShare = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("REST_TIME_" + (i + 1)));

                                        currentRest.appendChild(restName);
                                        switch (currentRestSelectionShare) {
                                            case 0:
                                                restName.appendChild(doc.createTextNode("Gummirast (35°-40°)"));
                                                break;
                                            case 1:
                                                restName.appendChild(doc.createTextNode("Weizenrast (45°)"));
                                                break;
                                            case 2:
                                                restName.appendChild(doc.createTextNode("Eiweißrast (57°)"));
                                                break;
                                            case 3:
                                                restName.appendChild(doc.createTextNode("Maltoserast (60°-65°)"));
                                                break;
                                            case 4:
                                                restName.appendChild(doc.createTextNode("Kombirast (66°-69°)"));
                                                break;
                                            case 5:
                                                restName.appendChild(doc.createTextNode("Verzuckerung (70°-75°)"));
                                                break;
                                            case 6:
                                                restName.appendChild(doc.createTextNode("Abmaischen (78°)"));
                                                break;
                                        }

                                        currentRest.appendChild(restTemp);
                                        restTemp.appendChild(doc.createTextNode(currentRestTempShare));

                                        currentRest.appendChild(restDuration);
                                        restDuration.appendChild(doc.createTextNode(currentRestTimeShare));
                                    }

                                    sud.appendChild(hops);

                                    for (int i = 0; i < hopAmountShare; i++) {
                                        hops.appendChild(doc.createElement("Anteil_" + (i + 1)));

                                        NodeList list = hops.getChildNodes();
                                        Element currentHop = (Element) list.item(i);

                                        Element hopName = doc.createElement("Name");
                                        Element hopTime = doc.createElement("Zeit");
                                        Element preWort = doc.createElement("Vorderwuerze");

                                        String currentHopCommentShare = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("HOPPING_COMMENT_" + (i + 1)));
                                        String currentHopDurationShare = Recipe.dbData.getString(Recipe.dbData.getColumnIndex("COOKING_DURATION_" + (i + 1)));

                                        currentHop.appendChild(hopName);
                                        hopName.appendChild(doc.createTextNode(currentHopCommentShare));

                                        currentHop.appendChild(hopTime);
                                        hopTime.appendChild(doc.createTextNode(currentHopDurationShare));

                                        currentHop.appendChild(preWort);
                                        preWort.appendChild(doc.createTextNode("0"));
                                    }

                                    TransformerFactory transformerFactory = TransformerFactory.newInstance();
                                    Transformer transformer = transformerFactory.newTransformer();
                                    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                                    DOMSource source = new DOMSource(doc);

                                    String filePath = Environment.getExternalStorageDirectory() + "/Braumeister/" + recipeNameShareFile + ".xsud.doc";
                                    StreamResult file = new StreamResult(new File(filePath));
                                    transformer.transform(source, file);

                                    String xsuddocExtension = filePath.substring(filePath.length() - 8, filePath.length());

                                    if (xsuddocExtension.equals("xsud.doc")) {
                                        File newFile = new File(filePath);
                                        Uri contentUri = getUriForFile(getApplicationContext(), "com.baviloworks.braumeister.fileprovider", newFile);

                                        Intent sendIntent = new Intent();
                                        sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                        sendIntent.setAction(Intent.ACTION_SEND);
                                        sendIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                                        sendIntent.setType("*/*");
                                        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.Main_Share_Send_To)));
                                    } else {
                                        Snackbar.make(parentLayout, R.string.Main_Not_Valid_Extension, Snackbar.LENGTH_SHORT).show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        })

                        .setNegativeButton(R.string.General_Notification_Button_Cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {

                            }
                        })

                        .show();
            } else {
                String recipeRequirementsTitle = getResources().getString(R.string.Recipe_Send_Recipe_Not_Complete_Title);
                SpannableString recipeRequirementsMessage = new SpannableString(getString(R.string.Recipe_Send_Recipe_Not_Complete_Message));

                Spanned s;

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    s = Html.fromHtml(String.valueOf(recipeRequirementsMessage), Html.FROM_HTML_MODE_LEGACY);
                } else {
                    //noinspection deprecation
                    s = Html.fromHtml(String.valueOf(recipeRequirementsMessage));
                }

                new AlertDialog.Builder(this)
                        .setPositiveButton(R.string.General_Notification_Button_Ok, null)
                        .setTitle(recipeRequirementsTitle)
                        .setMessage(s)
                        .show();
            }
        } else {
            Snackbar.make(parentLayout, R.string.Recipe_Share_No_Recipe_Available, Snackbar.LENGTH_SHORT).show();
        }
    }

    private class FileDownload extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... voids) {
            String filePath = "https://goo.gl/XgclO5";

            try {
                while (file.equals("")) {
                    if (isNetworkAvailable()) {
                        if (HttpRequest.get(filePath).ok()) {
                            File output = new File(getApplicationContext().getFilesDir().toString() + "/Test.png");
                            HttpRequest.get(filePath).receive(output);

                            if (output.exists()) {
                                try {
                                    file = Steg.withInput(output).decode().intoString();

                                    boolean isDeleted = output.delete();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            try {
                                Thread.sleep(60000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        runOnUiThread(new Thread(new Runnable() {
                            public void run() {
                                new AlertDialog.Builder(MainActivity.this)
                                        .setPositiveButton(R.string.General_Notification_Button_Ok, null)
                                        .setTitle(getString(R.string.Main_No_Internet_Title))
                                        .setMessage(getString(R.string.Main_No_Internet_Message))
                                        .show();
                            }
                        }));

                        return null;
                    }
                }
            } catch (HttpRequest.HttpRequestException exception) {
                exception.printStackTrace();
            }

            return null;
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = null;

        if (connectivityManager != null) {
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        }

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}