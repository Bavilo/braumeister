/*
 * Copyright (C) 2016 Braumeister by BaviloWorks <BaviloWorks@gmail.com>
 *
 * This work is licensed under the Creative Commons
 * Attribution-NonCommercial-ShareAlike 4.0 International License.
 *
 * You may not use this file except in compliance with the License.
 * To view a copy of this license, visit
 *
 *      http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baviloworks.braumeister;

import android.app.ActivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.baviloworks.braumeister.ConnectionServices.BluetoothService;
import com.baviloworks.braumeister.ConnectionServices.WiFiService;

public class DebuggingActivity extends AppCompatActivity {
    private View parentLayout;

    private Button FinishMashing, FinishSparging, FinishCooking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.debugging);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setTaskDescription(new ActivityManager.TaskDescription(null, null, ContextCompat.getColor(getApplicationContext(), R.color.Blue)));
        }

        Toolbar toolbar = findViewById(R.id.Toolbar);

        if (toolbar != null) {
            toolbar.setTitle(getString(R.string.Main_Menu_Debugging_Title));
            setSupportActionBar(toolbar);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        parentLayout = findViewById(android.R.id.content);

        FinishMashing = findViewById(R.id.finishMashing);
        FinishSparging = findViewById(R.id.finishSparging);
        FinishCooking = findViewById(R.id.finishCooking);

        FinishMashing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainActivity.bluetoothConnected || MainActivity.wifiConnected) {
                    if (Recipe.dataSent) {
                        if (MainActivity.bluetoothConnected) {
                            BluetoothService.sendMessage("A!");
                        } else {
                            WiFiService.sendMessage("A!");
                        }
                    } else {
                        Snackbar.make(parentLayout, R.string.Recipe_Send_Recipe_Not_Sent, Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Snackbar.make(parentLayout, R.string.Bluetooth_Disconnect_Not_Connected, Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        FinishSparging.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainActivity.bluetoothConnected || MainActivity.wifiConnected) {
                    if (Recipe.dataSent) {
                        if (MainActivity.bluetoothConnected) {
                            BluetoothService.sendMessage("B!");
                        } else {
                            WiFiService.sendMessage("B!");
                        }
                    } else {
                        Snackbar.make(parentLayout, R.string.Recipe_Send_Recipe_Not_Sent, Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Snackbar.make(parentLayout, R.string.Bluetooth_Disconnect_Not_Connected, Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        FinishCooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainActivity.bluetoothConnected || MainActivity.wifiConnected) {
                    if (Recipe.dataSent) {
                        if (MainActivity.bluetoothConnected) {
                            BluetoothService.sendMessage("C!");
                        } else {
                            WiFiService.sendMessage("C!");
                        }
                    } else {
                        Snackbar.make(parentLayout, R.string.Recipe_Send_Recipe_Not_Sent, Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Snackbar.make(parentLayout, R.string.Bluetooth_Disconnect_Not_Connected, Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}