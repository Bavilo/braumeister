/*
 * Copyright (C) 2016 Braumeister by BaviloWorks <BaviloWorks@gmail.com>
 *
 * This work is licensed under the Creative Commons
 * Attribution-NonCommercial-ShareAlike 4.0 International License.
 *
 * You may not use this file except in compliance with the License.
 * To view a copy of this license, visit
 *
 *      http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baviloworks.braumeister;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.baviloworks.braumeister.ConnectionServices.BluetoothService;
import com.baviloworks.braumeister.ConnectionServices.WiFiService;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import static android.app.Activity.RESULT_OK;

public class Recipe extends Fragment {
    private View view;

    private AlertDialog newRecipeAlertDialog;

    public static Cursor dbData;
    public static SimpleCursorAdapter cursorAdapter;

    private ImageButton addRest, removeRest, addHop, removeHop;
    private EditText mashingTemperature, cookingTime, spargeAmount;
    private Switch isVWHSwitch;
    public static FloatingActionButton addRecipe, deleteRecipe;

    private CardView[] restLayout, hopLayout;
    private Spinner[] restSelection;
    private EditText[] restTemperature, restTime, restComment, hopTime, hoppingComment;

    public static boolean dataSent = false;
    private boolean loadNotification = true;
    private boolean isUpdated = false;
    private int loadCount = 0;

    private String recipeName = "";
    private String isVWH = "false";

    private final int maxAmountOfRests = 7;
    private final int maxAmountOfHoppings = 7;

    private int currentAmountOfRests = 0;
    private int currentAmountOfHoppings = 0;

    private final int[] restSelectionXSUD = {0, 0, 0, 0, 0, 0, 0};
    private final int[] restTemperatureXSUD = {0, 0, 0, 0, 0, 0, 0};
    private final int[] restTimeXSUD = {0, 0, 0, 0, 0, 0, 0};
    private final int[] hopTimeXSUD = {0, 0, 0, 0, 0, 0, 0};

    private final String[] hoppingCommentXSUD = { "", "", "", "", "", "", "" };

    private int currentAmountOfRestsXSUD = 0;
    private int currentAmountOfHoppingsXSUD = 0;
    private int mashingTemperatureXSUD = 0;
    private float spargeAmountXSUD = 0f;
    private int cookingTimeXSUD = 0;

    private int hopIsVWH = 0;

    private final int FILE_SELECT_CODE_LOAD = 0;

    private NodeList nList;
    private Node node;
    private Document doc;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.recipe, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.Send:
                send();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem send = menu.findItem(R.id.Send);

        if (MainActivity.hideRecipeItems) {
            send.setEnabled(false);
            send.getIcon().setAlpha(128);
        } else {
            send.setEnabled(true);
            send.getIcon().setAlpha(255);
        }

        Objects.requireNonNull(getActivity()).invalidateOptionsMenu();
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.recipe, container, false);

        CardView mashingCard = view.findViewById(R.id.card_mashing);
        CardView lauteringCard = view.findViewById(R.id.card_lautering);
        CardView cookingCard = view.findViewById(R.id.card_cooking);

        addRecipe = view.findViewById(R.id.fab_add);
        deleteRecipe = view.findViewById(R.id.fab_delete);

        loadSpinnerData();

        SharedPreferences sp = Objects.requireNonNull(getActivity()).getSharedPreferences("prefs", Activity.MODE_PRIVATE);
        int recipeLoadPosition = sp.getInt("recipeLoadPosition", -1);
        MainActivity.recipeLoadSelection.setSelection(recipeLoadPosition);

        mashingCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                currentAmountOfRests = 0;

                if (dbData.getCount() > 0) {
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View convertView = inflater.inflate(R.layout.recipe_mashing, null);

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setView(convertView);

                    TextView title = new TextView(getActivity());
                    title.setText(dbData.getString(dbData.getColumnIndex("RECIPE_NAME")));
                    title.setBackgroundColor((ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.Green)));
                    title.setPadding(25, 40, 25, 40);
                    title.setGravity(Gravity.CENTER);
                    title.setTextColor(Color.WHITE);
                    title.setTextSize(24);

                    alertDialog.setCustomTitle(title);

                    restLayout = new CardView[maxAmountOfRests];
                    restSelection = new Spinner[maxAmountOfRests];
                    restTemperature = new EditText[maxAmountOfRests];
                    restTime = new EditText[maxAmountOfRests];
                    restComment = new EditText[maxAmountOfRests];

                    addRest = convertView.findViewById(R.id.addRest);
                    removeRest = convertView.findViewById(R.id.removeRest);
                    mashingTemperature = convertView.findViewById(R.id.mashTempEntry);

                    for (int i = 0; i < maxAmountOfRests; i++) {
                        int resID = getResources().getIdentifier("restLayout_" + (i + 1), "id", getActivity().getPackageName());
                        restLayout[i] = convertView.findViewById(resID);
                    }

                    for (int i = 0; i < maxAmountOfRests; i++) {
                        int resID = getResources().getIdentifier("restSelection_" + (i + 1), "id", getActivity().getPackageName());
                        restSelection[i] = convertView.findViewById(resID);
                    }

                    for (int i = 0; i < maxAmountOfRests; i++) {
                        int resID = getResources().getIdentifier("restTemperature_" + (i + 1), "id", getActivity().getPackageName());
                        restTemperature[i] = convertView.findViewById(resID);
                    }

                    for (int i = 0; i < maxAmountOfRests; i++) {
                        int resID = getResources().getIdentifier("restTime_" + (i + 1), "id", getActivity().getPackageName());
                        restTime[i] = convertView.findViewById(resID);
                    }

                    for (int i = 0; i < maxAmountOfRests; i++) {
                        int resID = getResources().getIdentifier("restComment_" + (i + 1), "id", getActivity().getPackageName());
                        restComment[i] = convertView.findViewById(resID);
                    }

                    mashingTemperature.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            if (editable != null && editable.length() == 3) {
                                editable.delete(0, 3);
                            } else {
                                if (mashingTemperature.getText().toString().equals("")) {
                                    for (int i = 0; i < maxAmountOfRests; i++) {
                                        restTime[i].setEnabled(false);
                                        restTemperature[i].setEnabled(false);
                                    }
                                } else {
                                    for (int i = 0; i < maxAmountOfHoppings; i++) {
                                        restTime[i].setEnabled(true);
                                        restTemperature[i].setEnabled(true);
                                    }
                                }
                            }
                        }
                    });

                    for (int i = 0; i < maxAmountOfRests; i++) {
                        restTemperature[i].addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void afterTextChanged(Editable editable) {
                                if (editable != null && editable.length() == 3) {
                                    editable.delete(0, 3);
                                }
                            }
                        });
                    }

                    for (int i = 0; i < maxAmountOfRests; i++) {
                        final int finalI = i;

                        restTime[i].addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void afterTextChanged(Editable editable) {
                                if (editable != null && editable.length() == 3) {
                                    editable.delete(0, 3);
                                } else {
                                    if (!restTime[finalI].getText().toString().equals("")) {
                                        if (Float.parseFloat(restTime[finalI].getText().toString()) > 90) {
                                            restTime[finalI].setText("90");
                                        }
                                    }
                                }
                            }
                        });
                    }

                    addRest.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (currentAmountOfRests < maxAmountOfRests) {
                                restLayout[currentAmountOfRests].setVisibility(View.VISIBLE);
                                restTemperature[currentAmountOfRests].requestFocus();

                                currentAmountOfRests++;
                            }
                        }
                    });

                    removeRest.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View view, MotionEvent motionEvent) {
                            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                                if (currentAmountOfRests > 0) {
                                    restLayout[currentAmountOfRests - 1].setVisibility(View.GONE);
                                    restSelection[currentAmountOfRests - 1].setSelection(0);
                                    restTemperature[currentAmountOfRests - 1].setText(null);
                                    restTime[currentAmountOfRests - 1].setText(null);
                                    restComment[currentAmountOfRests - 1].setText(null);

                                    currentAmountOfRests--;
                                }

                                if (currentAmountOfRests == 0) {
                                    mashingTemperature.setText(null);
                                }
                            } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                                if (currentAmountOfRests > 0) {
                                    restTemperature[currentAmountOfRests - 1].requestFocus();
                                }
                            }

                            return false;
                        }
                    });

                    alertDialog.setPositiveButton(R.string.General_Notification_Button_Save, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {
                            int countFlag = 0;

                            for (int i = 0; i < currentAmountOfRests; i++) {
                                if (restTime[i].getText().toString().equals("")) {
                                    countFlag++;
                                }

                                if (restTemperature[i].getText().toString().equals("")) {
                                    countFlag++;
                                }
                            }

                            if (countFlag == 0) {
                                saveMashingData();
                            } else {
                                Snackbar.make(view, R.string.Recipe_Save_Recipe_Error_Empty, Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    });

                    alertDialog.setNeutralButton(R.string.General_Notification_Button_Cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {

                        }
                    });

                    loadMashingData();

                    alertDialog.show();
                } else {
                    Snackbar.make(view, R.string.Recipe_Delete_No_Recipes, Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        lauteringCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (dbData.getCount() > 0) {
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View convertView = inflater.inflate(R.layout.recipe_sparging, null);

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setView(convertView);

                    TextView title = new TextView(getActivity());
                    title.setText(dbData.getString(dbData.getColumnIndex("RECIPE_NAME")));
                    title.setBackgroundColor((ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.Green)));
                    title.setPadding(25, 40, 25, 40);
                    title.setGravity(Gravity.CENTER);
                    title.setTextColor(Color.WHITE);
                    title.setTextSize(24);

                    alertDialog.setCustomTitle(title);

                    spargeAmount = convertView.findViewById(R.id.spargeAmountEntry);

                    spargeAmount.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            if (editable != null && editable.length() == 5) {
                                editable.delete(0, 5);
                            }
                        }
                    });

                    alertDialog.setPositiveButton(R.string.General_Notification_Button_Save, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {
                            int countFlag = 0;

                            if (spargeAmount.getText().toString().equals("")) {
                                countFlag++;
                            }

                            if (countFlag == 0) {
                                saveLauteringData();
                            } else {
                                Snackbar.make(view, R.string.Recipe_Save_Recipe_Error_Empty, Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    });

                    alertDialog.setNeutralButton(R.string.General_Notification_Button_Cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {

                        }
                    });

                    loadLauteringData();

                    alertDialog.show();
                } else {
                    Snackbar.make(view, R.string.Recipe_Delete_No_Recipes, Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        cookingCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                currentAmountOfHoppings = 0;

                if (dbData.getCount() > 0) {
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View convertView = inflater.inflate(R.layout.recipe_cooking, null);

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setView(convertView);

                    TextView title = new TextView(getActivity());
                    title.setText(dbData.getString(dbData.getColumnIndex("RECIPE_NAME")));
                    title.setBackgroundColor((ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.Green)));
                    title.setPadding(25, 40, 25, 40);
                    title.setGravity(Gravity.CENTER);
                    title.setTextColor(Color.WHITE);
                    title.setTextSize(24);

                    alertDialog.setCustomTitle(title);

                    hopLayout = new CardView[maxAmountOfHoppings];
                    hopTime = new EditText[maxAmountOfHoppings];
                    hoppingComment = new EditText[maxAmountOfHoppings];

                    addHop = convertView.findViewById(R.id.addHop);
                    removeHop = convertView.findViewById(R.id.removeHop);
                    cookingTime = convertView.findViewById(R.id.cookingTimeEntry);
                    isVWHSwitch = convertView.findViewById(R.id.isVWH);

                    for (int i = 0; i < maxAmountOfHoppings; i++) {
                        int resID = getResources().getIdentifier("hopLayout_" + (i + 1), "id", getActivity().getPackageName());
                        hopLayout[i] = convertView.findViewById(resID);
                    }

                    for (int i = 0; i < maxAmountOfHoppings; i++) {
                        int resID = getResources().getIdentifier("hopTime_" + (i + 1), "id", getActivity().getPackageName());
                        hopTime[i] = convertView.findViewById(resID);
                    }

                    for (int i = 0; i < maxAmountOfHoppings; i++) {
                        int resID = getResources().getIdentifier("hoppingComment_" + (i + 1), "id", getActivity().getPackageName());
                        hoppingComment[i] = convertView.findViewById(resID);
                    }

                    for (int i = 0; i < maxAmountOfHoppings; i++) {
                        final int finalI = i;
                        hopTime[finalI].addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void afterTextChanged(Editable editable) {
                                if (editable != null && editable.length() == 3) {
                                    editable.delete(0, 3);
                                } else {
                                    if (!hopTime[finalI].getText().toString().equals("")) {
                                        if (Integer.parseInt(hopTime[finalI].getText().toString()) > Integer.parseInt(cookingTime.getText().toString())) {
                                            hopTime[finalI].setText(cookingTime.getText().toString());
                                        }
                                    }
                                }
                            }
                        });
                    }

                    cookingTime.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            if (editable != null && editable.length() == 3) {
                                editable.delete(0, 3);
                            } else {
                                if (cookingTime.getText().toString().equals("")) {
                                    for (int i = 0; i < maxAmountOfHoppings; i++) {
                                        hopTime[i].setEnabled(false);
                                    }
                                } else {
                                    if (Float.parseFloat(cookingTime.getText().toString()) > 90) {
                                        cookingTime.setText("90");
                                    }

                                    for (int i = 0; i < maxAmountOfHoppings; i++) {
                                        hopTime[i].setEnabled(true);
                                    }
                                }
                            }
                        }
                    });

                    addHop.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (currentAmountOfHoppings < maxAmountOfHoppings) {
                                hopLayout[currentAmountOfHoppings].setVisibility(View.VISIBLE);
                                hopTime[currentAmountOfHoppings].requestFocus();

                                currentAmountOfHoppings++;
                            }
                        }
                    });

                    removeHop.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View view, MotionEvent motionEvent) {
                            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                                if (currentAmountOfHoppings > 0) {
                                    hopLayout[currentAmountOfHoppings - 1].setVisibility(View.GONE);
                                    hopTime[currentAmountOfHoppings - 1].setText(null);
                                    hoppingComment[currentAmountOfHoppings - 1].setText(null);

                                    currentAmountOfHoppings--;
                                }

                                if (currentAmountOfHoppings == 0) {
                                    cookingTime.setText(null);
                                }
                            } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                                if (currentAmountOfHoppings > 0) {
                                    hopTime[currentAmountOfHoppings - 1].requestFocus();
                                }
                            }

                            return false;
                        }
                    });

                    isVWHSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if (b) {
                                isVWH = "true";
                            } else {
                                isVWH = "false";
                            }
                        }
                    });

                    alertDialog.setPositiveButton(R.string.General_Notification_Button_Save, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {
                            boolean duplicate = false;
                            boolean ascending = false;

                            int countFlag = 0;

                            int oldValue;
                            int newValue;

                            for (int i = 0; i < currentAmountOfHoppings; i++) {
                                if (hopTime[i].getText().toString().equals("")) {
                                    countFlag++;
                                }
                            }

                            Set<String> store = new HashSet<>();

                            for (int i = 0; i < currentAmountOfHoppings; i++) {
                                if (!store.add(hopTime[i].getText().toString())) {
                                    duplicate = true;
                                }
                            }

                            for (int i = 0; i < currentAmountOfHoppings - 1; i++) {
                                if (!hopTime[i].getText().toString().equals("") && !hopTime[i + 1].getText().toString().equals("")) {
                                    oldValue = Integer.parseInt(hopTime[i].getText().toString());
                                    newValue = Integer.parseInt(hopTime[i + 1].getText().toString());

                                    if (oldValue < newValue) {
                                        ascending = true;
                                    }
                                }
                            }

                            if (countFlag == 0) {
                                if (!duplicate) {
                                    if (!ascending) {
                                        saveCookingData();
                                    } else {
                                        Snackbar.make(view, R.string.Recipe_Save_Recipe_Error_Ascending, Snackbar.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Snackbar.make(view, R.string.Recipe_Save_Recipe_Error_Duplicate, Snackbar.LENGTH_SHORT).show();
                                }
                            } else {
                                Snackbar.make(view, R.string.Recipe_Save_Recipe_Error_Empty, Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    });

                    alertDialog.setNeutralButton(R.string.General_Notification_Button_Cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {

                        }
                    });

                    loadCookingData();

                    alertDialog.show();
                } else {
                    Snackbar.make(view, R.string.Recipe_Delete_No_Recipes, Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        addRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addEmptyRecipe();
            }
        });

        deleteRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteRecipe();
            }
        });


        return view;
    }

    private void addEmptyRecipe() {
        Button button = new Button(getActivity());
        button.setText(getText(R.string.Recipe_Add_Recipe_Button_Text));

        TextView text = new TextView(getActivity());
        text.setTextSize(18);
        text.setText(getText(R.string.Recipe_Add_Recipe_TextView_Text));
        text.setPadding(0, 100, 0, 60);
        text.setGravity(Gravity.CENTER_HORIZONTAL);

        final EditText input = new EditText(getActivity());
        input.setHint(R.string.Recipe_Layout_Recipe_Name_Hint);
        input.setSingleLine(true);
        input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(25)});
        input.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        input.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.DarkGreen));

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        layout.setPadding(60, 150, 60, 0);
        layout.addView(button);
        layout.addView(text);
        layout.addView(input);

        AlertDialog.Builder newRecipe = new AlertDialog.Builder(getActivity());
        newRecipe.setTitle(R.string.Recipe_Add_Recipe_Name_Title);
        newRecipe.setView(layout);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showFileChooser();

                newRecipeAlertDialog.dismiss();
            }
        });

        newRecipe.setPositiveButton(R.string.General_Notification_Button_Ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                if (!input.getText().toString().isEmpty()) {
                    boolean isUnique = false;

                    if ((getActivity()) != null) {
                        isUnique = ((MainActivity) getActivity()).database.checkIfDataExists(input.getText().toString());
                    }

                    if (isUnique) {
                        ((MainActivity) getActivity()).database.insertEmptyData(input.getText().toString());
                        refresh();

                        Snackbar.make(view, R.string.Recipe_Save_Recipe_Inserted, Snackbar.LENGTH_LONG)
                                .setAction(R.string.Recipe_Save_Undo, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        ((MainActivity) getActivity()).database.deleteData(input.getText().toString());
                                        refresh();
                                    }
                                }).show();
                    } else {
                        Snackbar.make(view, R.string.Recipe_Add_Recipe_Already_Exists, Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Snackbar.make(view, R.string.Recipe_Save_Recipe_No_Name, Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        newRecipe.setNegativeButton(R.string.General_Notification_Button_Cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });

        newRecipeAlertDialog = newRecipe.show();
    }

    private void saveMashingData() {
        final String name = dbData.getString(dbData.getColumnIndex("RECIPE_NAME"));

        if ((getActivity()) != null) {
            isUpdated = ((MainActivity) getActivity()).database.updateMashingData(name, currentAmountOfRests, mashingTemperature, restSelection, restTemperature, restTime, restComment);
        }

        refresh();

        Snackbar.make(view, R.string.Recipe_Save_Recipe_Updated, Snackbar.LENGTH_SHORT).show();
    }

    private void saveLauteringData() {
        final String name = dbData.getString(dbData.getColumnIndex("RECIPE_NAME"));

        if ((getActivity()) != null) {
            isUpdated = ((MainActivity) getActivity()).database.updateLauteringData(name, spargeAmount);
        }

        refresh();

        Snackbar.make(view, R.string.Recipe_Save_Recipe_Updated, Snackbar.LENGTH_SHORT).show();
    }

    private void saveCookingData() {
        final String name = dbData.getString(dbData.getColumnIndex("RECIPE_NAME"));

        if ((getActivity()) != null) {
            isUpdated = ((MainActivity) getActivity()).database.updateCookingData(name, currentAmountOfHoppings, cookingTime, hopTime, hoppingComment, isVWH);
        }

        refresh();

        Snackbar.make(view, R.string.Recipe_Save_Recipe_Updated, Snackbar.LENGTH_SHORT).show();
    }

    private void loadSpinnerData() {
        if ((getActivity()) != null) {
            dbData = ((MainActivity) getActivity()).database.getAllData();
        }

        String[] recipeNames = new String[]{Database.COL_2};
        int[] recipeTo = new int[]{android.R.id.text1};

        cursorAdapter = new SimpleCursorAdapter(Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).getThemedContext(), android.R.layout.simple_spinner_item, dbData, recipeNames, recipeTo, 0);
        cursorAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        MainActivity.recipeLoadSelection.setAdapter(cursorAdapter);

        MainActivity.recipeLoadSelection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View v, int pos, long id) {
                loadCount++;

                SharedPreferences sp = getActivity().getSharedPreferences("prefs", Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putInt("recipeLoadPosition", MainActivity.recipeLoadSelection.getSelectedItemPosition());
                editor.apply();

                dbData = (Cursor) cursorAdapter.getItem(pos);

                if (loadCount > 1 && loadNotification) {
                    Snackbar.make(view, R.string.Recipe_Load_Recipe_Loaded, Snackbar.LENGTH_SHORT).show();
                }

                loadNotification = true;
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadMashingData() {
        dbData = (Cursor) cursorAdapter.getItem(MainActivity.recipeLoadSelection.getSelectedItemPosition());

        currentAmountOfRests = Integer.parseInt(dbData.getString(dbData.getColumnIndex("AMOUNT_OF_RESTS")));
        mashingTemperature.setText(dbData.getString(dbData.getColumnIndex("MASHING_TEMPERATURE")));

        for (int i = 0; i < currentAmountOfRests; i++) {
            restSelection[i].setSelection(Integer.parseInt(dbData.getString(dbData.getColumnIndex("REST_SELECTION_" + (i + 1)))));
        }

        for (int i = 0; i < currentAmountOfRests; i++) {
            restTemperature[i].setText(dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_" + (i + 1))));
        }

        for (int i = 0; i < currentAmountOfRests; i++) {
            restTime[i].setText(dbData.getString(dbData.getColumnIndex("REST_TIME_" + (i + 1))));
        }

        for (int i = 0; i < currentAmountOfRests; i++) {
            restComment[i].setText(dbData.getString(dbData.getColumnIndex("REST_COMMENT_" + (i + 1))));
        }

        for (int i = 0; i < maxAmountOfRests; i++) {
            restLayout[i].setVisibility(View.GONE);
        }

        for (int i = 0; i < currentAmountOfRests; i++) {
            restLayout[i].setVisibility(View.VISIBLE);
        }
    }

    private void loadLauteringData() {
        dbData = (Cursor) cursorAdapter.getItem(MainActivity.recipeLoadSelection.getSelectedItemPosition());

        spargeAmount.setText(dbData.getString(dbData.getColumnIndex("SPARGE_AMOUNT")));
    }

    private void loadCookingData() {
        dbData = (Cursor) cursorAdapter.getItem(MainActivity.recipeLoadSelection.getSelectedItemPosition());

        currentAmountOfHoppings = Integer.parseInt(dbData.getString(dbData.getColumnIndex("AMOUNT_OF_HOPPINGS")));
        cookingTime.setText(dbData.getString(dbData.getColumnIndex("TOTAL_COOKING_DURATION")));
        isVWH = dbData.getString(dbData.getColumnIndex("IS_VWH"));

        if (isVWH.equals("true")) {
            isVWHSwitch.setChecked(true);
        } else {
            isVWHSwitch.setChecked(false);
        }

        for (int i = 0; i < currentAmountOfHoppings; i++) {
            hopTime[i].setText(dbData.getString(dbData.getColumnIndex("COOKING_DURATION_" + (i + 1))));
        }

        for (int i = 0; i < currentAmountOfHoppings; i++) {
            hoppingComment[i].setText(dbData.getString(dbData.getColumnIndex("HOPPING_COMMENT_" + (i + 1))));
        }

        for (int i = 0; i < maxAmountOfHoppings; i++) {
            hopLayout[i].setVisibility(View.GONE);
        }

        for (int i = 0; i < currentAmountOfHoppings; i++) {
            hopLayout[i].setVisibility(View.VISIBLE);
        }
    }

    private void deleteRecipe() {
        if (dbData.getCount() > 0) {
            dbData = (Cursor) cursorAdapter.getItem(MainActivity.recipeLoadSelection.getSelectedItemPosition());

            final String name = dbData.getString(dbData.getColumnIndex("RECIPE_NAME"));
            String deleteTitle = getResources().getString(R.string.Recipe_Delete_Title_Delete);
            String deleteMessage = String.format(getResources().getString(R.string.Recipe_Delete_Message_Delete), name);

            new AlertDialog.Builder(getActivity())
                    .setTitle(deleteTitle)
                    .setMessage(deleteMessage)
                    .setPositiveButton(R.string.General_Notification_Button_Ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if ((getActivity()) != null) {
                                ((MainActivity) getActivity()).database.deleteData(name);
                            }

                            Snackbar.make(view, R.string.Recipe_Delete_Recipe_Deleted, Snackbar.LENGTH_SHORT).show();
                            refresh();

                            currentAmountOfRests = 0;
                        }
                    })

                    .setNegativeButton(R.string.General_Notification_Button_Cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int whichButton) {

                        }
                    })

                    .show();
        } else {
            Snackbar.make(view, R.string.Recipe_Delete_No_Recipes, Snackbar.LENGTH_SHORT).show();
        }
    }

    private void refresh() {
        Objects.requireNonNull(getActivity()).runOnUiThread(new Thread(new Runnable() {
            public void run() {
                if (dbData != null) {
                    if ((getActivity()) != null) {
                        dbData = ((MainActivity) getActivity()).database.getAllData();
                    }

                    cursorAdapter.changeCursor(dbData);

                    if (!isUpdated) {
                        MainActivity.recipeLoadSelection.setSelection(cursorAdapter.getCount() - 1);
                    }

                    if (dbData.getCount() == 0) {
                        MainActivity.recipeLoadSelection.setVisibility(View.GONE);
                    } else {
                        MainActivity.recipeLoadSelection.setVisibility(View.VISIBLE);
                    }
                }

                isUpdated = false;
                loadNotification = false;
            }
        }));
    }

    private void send() {
        String disconnectTitle = getResources().getString(R.string.Recipe_Send_Send_Title);
        SpannableString disconnectMessage = new SpannableString(getString(R.string.Recipe_Send_Send_Message));

        Spanned s;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            s = Html.fromHtml(String.valueOf(disconnectMessage), Html.FROM_HTML_MODE_LEGACY);
        } else {
            //noinspection deprecation
            s = Html.fromHtml(String.valueOf(disconnectMessage));
        }

        if (MainActivity.bluetoothConnected || MainActivity.wifiConnected) {
            if (dbData.getCount() > 0) {
                dbData = (Cursor) cursorAdapter.getItem(MainActivity.recipeLoadSelection.getSelectedItemPosition());

                String minRequirementMashingTemp = dbData.getString(dbData.getColumnIndex("MASHING_TEMPERATURE"));
                String minRequirementRestTempOne = dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_1"));
                String minRequirementRestTimeOne = dbData.getString(dbData.getColumnIndex("REST_TIME_1"));
                String minRequirementRestTempTwo = dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_2"));
                String minRequirementRestTimeTwo = dbData.getString(dbData.getColumnIndex("REST_TIME_2"));
                String minRequirementCookingDuration = dbData.getString(dbData.getColumnIndex("TOTAL_COOKING_DURATION"));
                String minRequirementHopTimeOne = dbData.getString(dbData.getColumnIndex("COOKING_DURATION_1"));

                if (!minRequirementMashingTemp.equals("") && !minRequirementRestTempOne.equals("") &&
                        !minRequirementRestTimeOne.equals("") && !minRequirementRestTempTwo.equals("") &&
                        !minRequirementRestTimeTwo.equals("") && !minRequirementCookingDuration.equals("") &&
                        !minRequirementHopTimeOne.equals("")) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(disconnectTitle)
                            .setMessage(s)

                            .setPositiveButton(R.string.General_Notification_Button_Ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    if (MainActivity.bluetoothConnected) {
                                        BluetoothService.sendMessage(
                                                "I" + dbData.getString(dbData.getColumnIndex("AMOUNT_OF_RESTS")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("MASHING_TEMPERATURE")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_1")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_2")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_3")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_4")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_5")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_6")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_7")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TIME_1")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TIME_2")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TIME_3")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TIME_4")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TIME_5")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TIME_6")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TIME_7")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("SPARGE_AMOUNT")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("AMOUNT_OF_HOPPINGS")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("TOTAL_COOKING_DURATION")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("COOKING_DURATION_1")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("COOKING_DURATION_2")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("COOKING_DURATION_3")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("COOKING_DURATION_4")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("COOKING_DURATION_5")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("COOKING_DURATION_6")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("COOKING_DURATION_7")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("IS_VWH")) + "!"
                                        );
                                    } else {
                                        WiFiService.sendMessage(
                                                "I" + dbData.getString(dbData.getColumnIndex("AMOUNT_OF_RESTS")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("MASHING_TEMPERATURE")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_1")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_2")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_3")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_4")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_5")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_6")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TEMPERATURE_7")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TIME_1")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TIME_2")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TIME_3")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TIME_4")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TIME_5")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TIME_6")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("REST_TIME_7")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("SPARGE_AMOUNT")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("AMOUNT_OF_HOPPINGS")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("TOTAL_COOKING_DURATION")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("COOKING_DURATION_1")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("COOKING_DURATION_2")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("COOKING_DURATION_3")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("COOKING_DURATION_4")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("COOKING_DURATION_5")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("COOKING_DURATION_6")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("COOKING_DURATION_7")) + ","
                                                        + dbData.getString(dbData.getColumnIndex("IS_VWH")) + "!"
                                        );
                                    }

                                    MainActivity.mViewPager.setCurrentItem(2);

                                    Snackbar.make(view, R.string.Recipe_Send_Data_Sent, Snackbar.LENGTH_SHORT).show();
                                }
                            })

                            .setNegativeButton(R.string.General_Notification_Button_Cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int whichButton) {

                                }
                            })

                            .show();
                } else {
                    String recipeRequirementsTitle = getResources().getString(R.string.Recipe_Send_Recipe_Not_Complete_Title);
                    SpannableString recipeRequirementsMessage = new SpannableString(getString(R.string.Recipe_Send_Recipe_Not_Complete_Message));

                    Spanned s2;

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        s2 = Html.fromHtml(String.valueOf(recipeRequirementsMessage), Html.FROM_HTML_MODE_LEGACY);
                    } else {
                        //noinspection deprecation
                        s2 = Html.fromHtml(String.valueOf(recipeRequirementsMessage));
                    }

                    new AlertDialog.Builder(getActivity())
                            .setPositiveButton(R.string.General_Notification_Button_Ok, null)
                            .setTitle(recipeRequirementsTitle)
                            .setMessage(s2)
                            .show();
                }
            } else {
                Snackbar.make(view, R.string.Recipe_Send_No_Recipe_Available, Snackbar.LENGTH_SHORT).show();
            }
        } else {
            Snackbar.make(view, R.string.Bluetooth_Disconnect_Not_Connected, Snackbar.LENGTH_SHORT).show();
        }
    }

    private void showFileChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Objects.requireNonNull(getActivity()).checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setTitle(R.string.Main_Need_Write_External_Storage_Permission_Title);
                builder.setMessage(R.string.Main_Need_Write_External_Storage_Permission_Message);
                builder.setPositiveButton(R.string.General_Notification_Button_Ok, null);

                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        requestPermissions(new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE }, MainActivity.PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
                    }
                });

                builder.show();
            } else {
                startFileChooserIntent();
            }
        } else {
            startFileChooserIntent();
        }
    }

    private void startFileChooserIntent() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(intent, FILE_SELECT_CODE_LOAD);
        } catch (ActivityNotFoundException ex) {
            Snackbar.make(view, R.string.Main_Install_File_Manager, Snackbar.LENGTH_LONG).show();
        }
    }

    private static String getContentFromSegments(List<String> segments) {
        String contentPath = "";

        for(String item : segments) {
            if (item.startsWith("content://")) {
                contentPath = item;
                break;
            }
        }

        return contentPath;
    }

    private static boolean fileExists(String filePath) {
        File file = new File(filePath);

        return file.exists();
    }

    private static String getPathFromExtSD(String[] pathData) {
        final String type = pathData[0];
        final String relativePath = "/" + pathData[1];
        String fullPath;

        if ("primary".equalsIgnoreCase(type)) {
            fullPath = Environment.getExternalStorageDirectory() + relativePath;
            if (fileExists(fullPath)) {
                return fullPath;
            }
        }

        fullPath = System.getenv("SECONDARY_STORAGE") + relativePath;

        if (fileExists(fullPath)) {
            return fullPath;
        }

        fullPath = System.getenv("EXTERNAL_STORAGE") + relativePath;

        if (fileExists(fullPath)) {
            return fullPath;
        }

        return fullPath;
    }

    private static String getPath(final Context context, final Uri uri) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (DocumentsContract.isDocumentUri(context, uri)) {
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");

                    String fullPath = getPathFromExtSD(split);

                    if (!fullPath.equals("")) {
                        return fullPath;
                    } else {
                        return null;
                    }
                } else if (isDownloadsDocument(uri)) {
                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                    return getDataColumn(context, contentUri, null, null);
                } else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;

                    switch (type) {
                        case "image":
                            contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                            break;
                        case "video":
                            contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                            break;
                        case "audio":
                            contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                            break;
                    }

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{split[1]};

                    return getDataColumn(context, contentUri, selection, selectionArgs);
                } else if (isGoogleDriveUri(uri)) {
                    return "cloud";
                }
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            if (isGooglePhotosUri(uri)) {
                String contentPath = getContentFromSegments(uri.getPathSegments());

                if (!contentPath.equals("")) {
                    return getPath(context, Uri.parse(contentPath));
                } else {
                    return null;
                }
            }

            return getDataColumn(context, uri, null, null);
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = { column };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return null;
    }

    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private static boolean isGooglePhotosUri(Uri uri) {
        return ("com.google.android.apps.photos.content".equals(uri.getAuthority())
                || "com.google.android.apps.photos.contentprovider".equals(uri.getAuthority()));
    }

    private static boolean isGoogleDriveUri(Uri uri) {
        return "com.google.android.apps.docs.storage".equals(uri.getAuthority());
    }

    private class parseXML extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            try {
                currentAmountOfRestsXSUD = 0;
                currentAmountOfHoppingsXSUD = 0;

                File file = new File(params[0]);

                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                doc = dBuilder.parse(file);
                doc.getDocumentElement().normalize();

                getRecipeName("Sud", "Sudname", "Sudname does not exist");

                boolean isUnique = false;
                if ((getActivity()) != null) {
                    isUnique = ((MainActivity) getActivity()).database.checkIfDataExists(recipeName);
                }

                if (isUnique) {
                    getMashingTemp("Sud", "EinmaischenTemp", "EinmaischenTemp does not exist");
                    getSpargingAmount("Sud", "erg_WNachguss", "erg_WNachguss does not exist");
                    getCookingDuration("Sud", "KochdauerNachBitterhopfung", "KochdauerNachBitterhopfung does not exist");
                    getIsVWH("Sud", "VWH", "VWH does not exist");

                    for (int i = 0; i < maxAmountOfRests; i++) {
                        getRestValues("Rast_" + (i + 1), "RastName", "Rast_" + (i + 1) + " " + "does not exist");
                    }

                    for (int i = 1, j = 1; i < maxAmountOfRests * 2; i += 2, j++) {
                        getHopValues("Hopfengaben", "Vorderwuerze", i, "Anteil_" + j + " " + "does not exist");
                    }

                    ((MainActivity) getActivity()).database.insertFullData(recipeName, currentAmountOfRestsXSUD, mashingTemperatureXSUD, restSelectionXSUD, restTemperatureXSUD, restTimeXSUD, spargeAmountXSUD, currentAmountOfHoppingsXSUD, cookingTimeXSUD, hopTimeXSUD, hoppingCommentXSUD, isVWH);
                    refresh();

                    Snackbar.make(view, R.string.Recipe_Save_Recipe_Inserted, Snackbar.LENGTH_LONG)
                            .setAction(R.string.Recipe_Save_Undo, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ((MainActivity) getActivity()).database.deleteData(recipeName);
                                    refresh();
                                }
                            }).show();
                } else {
                    Snackbar.make(view, R.string.Recipe_Add_Recipe_Already_Exists, Snackbar.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    private static String getXMLValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);

        return node.getNodeValue();
    }

    private void getRecipeName(String tagName, String valueName, String error) {
        try {
            nList = doc.getElementsByTagName(tagName);
            node = nList.item(0);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element value = (Element) node;

                recipeName = getXMLValue(valueName, value);
            }
        } catch (Exception e) {
            System.out.println(error);
        }
    }

    private void getMashingTemp(String tagName, String valueName, String error) {
        try {
            nList = doc.getElementsByTagName(tagName);
            node = nList.item(0);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element value = (Element) node;

                mashingTemperatureXSUD = Integer.valueOf(getXMLValue(valueName, value));
            }
        } catch (Exception e) {
            System.out.println(error);
        }
    }

    private void getRestValues(String tagName, String valueName, String error) {
        try {
            nList = doc.getElementsByTagName(tagName);
            node = nList.item(0);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element value = (Element) node;

                String mashType = getXMLValue(valueName, value);

                switch (mashType) {
                    case "Gummirast (35°-40°)":
                        restSelectionXSUD[currentAmountOfRestsXSUD] = 0;
                        break;
                    case "Weizenrast (45°)":
                        restSelectionXSUD[currentAmountOfRestsXSUD] = 1;
                        break;
                    case "Eiweißrast (57°)":
                        restSelectionXSUD[currentAmountOfRestsXSUD] = 2;
                        break;
                    case "Maltoserast (60°-65°)":
                        restSelectionXSUD[currentAmountOfRestsXSUD] = 3;
                        break;
                    case "Kombirast (66°-69°)":
                        restSelectionXSUD[currentAmountOfRestsXSUD] = 4;
                        break;
                    case "Verzuckerung (70°-75°)":
                        restSelectionXSUD[currentAmountOfRestsXSUD] = 5;
                        break;
                    case "Abmaischen (78°)":
                        restSelectionXSUD[currentAmountOfRestsXSUD] = 6;
                        break;
                }

                restTemperatureXSUD[currentAmountOfRestsXSUD] = Integer.valueOf(getXMLValue("RastTemp", value));
                restTimeXSUD[currentAmountOfRestsXSUD] = Integer.valueOf(getXMLValue("RastDauer", value));

                currentAmountOfRestsXSUD++;
            }
        } catch (Exception e) {
            System.out.println(error);
        }
    }

    private void getSpargingAmount(String tagName, String valueName, String error) {
        try {
            nList = doc.getElementsByTagName(tagName);
            node = nList.item(0);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element value = (Element) node;

                spargeAmountXSUD = Float.valueOf(getXMLValue(valueName, value));

                DecimalFormat df = new DecimalFormat("#.#");
                String tempSpargeAmount = df.format(spargeAmountXSUD);

                spargeAmountXSUD = Float.valueOf(tempSpargeAmount.replace(",", "."));
            }
        } catch (Exception e) {
            System.out.println(error);
        }
    }

    private void getCookingDuration(String tagName, String valueName, String error) {
        try {
            nList = doc.getElementsByTagName(tagName);
            node = nList.item(0);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element value = (Element) node;

                cookingTimeXSUD = Integer.valueOf(getXMLValue(valueName, value));
            }
        } catch (Exception e) {
            System.out.println(error);
        }
    }

    private void getIsVWH(String tagName, String valueName, String error) {
        try {
            nList = doc.getElementsByTagName(tagName);
            node = nList.item(0);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element value = (Element) node;

                String vwhValue = getXMLValue(valueName, value);

                if (vwhValue.equals("true")) {
                    isVWH = "true";
                } else {
                    isVWH = "false";
                }
            }
        } catch (Exception e) {
            isVWH = "false";

            System.out.println(error);
        }
    }

    private void getHopValues(String tagName, String valueName, int i, String error) {
        try {
            nList = doc.getElementsByTagName(tagName);
            node = nList.item(0);
            node = node.getChildNodes().item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element value = (Element) node;

                hopIsVWH = Integer.valueOf(getXMLValue(valueName, value));

                if (hopIsVWH == 1) {
                    isVWH = "true";
                } else {
                    hopTimeXSUD[currentAmountOfHoppingsXSUD] = Integer.valueOf(getXMLValue("Zeit", value));

                    try {
                        hoppingCommentXSUD[currentAmountOfHoppingsXSUD] = getXMLValue("Name", value);
                    } catch (Exception e) {
                        System.out.println("Comment_" + (currentAmountOfHoppingsXSUD + 1) + " does not exist");
                    }

                    currentAmountOfHoppingsXSUD++;
                }
            }
        } catch (Exception e) {
            System.out.println(error);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE_LOAD:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    String filePath = getPath(getActivity(), uri);

                    if (filePath != null) {
                        String xsudExtensionOne = filePath.substring(filePath.length() - 4, filePath.length());
                        String xsudExtensionTwo = filePath.substring(filePath.length() - 8, filePath.length() - 4);

                        if (xsudExtensionOne.equals("xsud") || xsudExtensionTwo.equals("xsud")) {
                            new parseXML().execute(filePath);
                        } else {
                            Snackbar.make(view, R.string.Main_Not_Valid_Extension, Snackbar.LENGTH_SHORT).show();
                        }
                    }
                }

                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}