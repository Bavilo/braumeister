/*
 * Copyright (C) 2016 Braumeister by BaviloWorks <BaviloWorks@gmail.com>
 *
 * This work is licensed under the Creative Commons
 * Attribution-NonCommercial-ShareAlike 4.0 International License.
 *
 * You may not use this file except in compliance with the License.
 * To view a copy of this license, visit
 *
 *      http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baviloworks.braumeister;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.baviloworks.braumeister.ConnectionServices.BluetoothService;
import com.baviloworks.braumeister.ConnectionServices.WiFiService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Connection extends Fragment {
    private static View view;

    public static TextView ConnectionStatus, BluetoothHeader, bluetoothDeviceNotFound;
    private TextView WifiHeader;
    public static ProgressDialog connectProgressBluetooth, disconnectProgressBluetooth, connectProgressWiFi, disconnectProgressWiFi;
    public static ProgressBar bluetoothScanProgressbar;
    public static ListView bluetoothDeviceList, wifiDeviceList;
    public static LinearLayout bluetoothListLayout;
    private EditText name, address;

    private final ArrayList<String> deviceList = new ArrayList<>();
    private final StringBuilder entries = new StringBuilder();

    public static boolean disconnectPressed = false;
    public static boolean connectPressedBluetooth = false;
    public static boolean connectPressedWiFi = false;
    public static boolean connectionIsAlive = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.connection, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.Scan:
                scanBluetooth();
                break;
            case R.id.Add:
                addWifi();
                break;
            case R.id.Disconnect:
                if (MainActivity.bluetoothConnected) {
                    disconnectBluetooth();
                } else if (MainActivity.wifiConnected) {
                    disconnectWiFi();
                } else {
                    Snackbar.make(view, R.string.Bluetooth_Disconnect_Not_Connected, Snackbar.LENGTH_SHORT).show();
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.connection, container, false);

        SharedPreferences pref = Objects.requireNonNull(getActivity()).getSharedPreferences("prefs", Activity.MODE_PRIVATE);
        String entry = pref.getString("wifiList", "");

        entries.append(entry);

        String[] splitEntries = entries.toString().split(",");

        deviceList.clear();

        if (!entry.isEmpty()) {
            Collections.addAll(deviceList, splitEntries);
        }

        bluetoothDeviceNotFound = view.findViewById(R.id.Bluetooth_Device_Not_Found);
        ConnectionStatus = view.findViewById(R.id.Connection_Status);
        WifiHeader = view.findViewById(R.id.WiFi_Header);
        BluetoothHeader = view.findViewById(R.id.Bluetooth_Header);
        bluetoothScanProgressbar = view.findViewById(R.id.Bluetooth_Scan_Progressbar);
        bluetoothDeviceList = view.findViewById(R.id.Bluetooth_Device_List);
        wifiDeviceList = view.findViewById(R.id.Wifi_Device_List);
        bluetoothListLayout = view.findViewById(R.id.Bluetooth_List_Layout);

        bluetoothDeviceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MainActivity.bluetoothAdapter.cancelDiscovery();

                connectBluetooth(position);
            }
        });

        wifiDeviceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                connectWiFi(position);
            }
        });

        wifiDeviceList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences pref = getActivity().getSharedPreferences("prefs", Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();

                String removedEntry = entries.toString().replace(wifiDeviceList.getItemAtPosition(i).toString() + ",", "");

                entries.delete(0, entries.length());
                entries.append(removedEntry);

                editor.putString("wifiList", entries.toString());
                editor.apply();

                String[] splitEntries = entries.toString().split(",");

                deviceList.clear();

                if (!removedEntry.isEmpty()) {
                    Collections.addAll(deviceList, splitEntries);
                } else {
                    WifiHeader.setVisibility(View.GONE);
                }

                wifiDeviceList.setAdapter(new ArrayAdapter<>(Objects.requireNonNull(getContext()), android.R.layout.simple_list_item_1, deviceList));

                return false;
            }
        });

        wifiDeviceList.setAdapter(new ArrayAdapter<>(Objects.requireNonNull(getContext()), android.R.layout.simple_list_item_1, deviceList));

        if (deviceList.isEmpty()) {
            WifiHeader.setVisibility(View.GONE);
        } else {
            WifiHeader.setVisibility(View.VISIBLE);
        }

        connectionIsAlive = true;

        return view;
    }

    private void scanBluetooth() {
        if (MainActivity.bluetoothAdapter != null) {
            if (!MainActivity.bluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, MainActivity.REQUEST_ENABLE_BT);
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (Objects.requireNonNull(getActivity()).checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                        builder.setTitle(R.string.Bluetooth_Scan_Need_Location_Permission_Title);
                        builder.setMessage(R.string.Bluetooth_Scan_Need_Location_Permission_Message);
                        builder.setPositiveButton(R.string.General_Notification_Button_Ok, null);

                        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                requestPermissions(new String[] { Manifest.permission.ACCESS_COARSE_LOCATION }, MainActivity.PERMISSION_REQUEST_COARSE_LOCATION);
                            }
                        });

                        builder.show();
                    } else {
                        startDiscover();
                    }
                } else {
                    startDiscover();
                }
            }
        } else {
            Snackbar.make(view, R.string.Main_Bluetooth_Not_Supported, Snackbar.LENGTH_SHORT).show();
        }
    }

    private void startDiscover() {
        if (!MainActivity.bluetoothConnected && !MainActivity.bluetoothAdapter.isDiscovering()) {
            bluetoothDeviceNotFound.setVisibility(View.GONE);
            bluetoothDeviceNotFound.setText(null);

            MainActivity.deviceList.clear();

            Connection.BluetoothHeader.setVisibility(View.GONE);
            Connection.bluetoothListLayout.setVisibility(View.GONE);

            MainActivity.bluetoothAdapter.startDiscovery();
            bluetoothScanProgressbar.setVisibility(View.VISIBLE);
        } else {
            Snackbar.make(view, R.string.Bluetooth_Scan_Error, Snackbar.LENGTH_SHORT).show();
        }
    }

    private void connectBluetooth(int position) {
        if (!MainActivity.bluetoothConnected) {
            String name = bluetoothDeviceList.getItemAtPosition(position).toString();
            String mac = name.substring(name.length() - 17, name.length());

            connectProgressBluetooth = ProgressDialog.show(getActivity(), getString(R.string.Bluetooth_Connect_Connect_Progress_Title), getString(R.string.Bluetooth_Connect_Connect_Progress_Message), true);

            connectPressedBluetooth = true;
            MainActivity.bluetoothDeviceFound = true;

            Intent intent = new Intent(getActivity(), BluetoothService.class);
            intent.putExtra(MainActivity.START_FLAG, "Start");
            intent.putExtra(MainActivity.MAC_ADDRESS, mac);
            Objects.requireNonNull(getActivity()).startService(intent);
        } else {
            Snackbar.make(view, R.string.Bluetooth_Connect_Already_Connected, Snackbar.LENGTH_SHORT).show();
        }
    }

    private void disconnectBluetooth() {
        String disconnectTitle = getResources().getString(R.string.Bluetooth_Disconnect_Disconnect_Title);
        String disconnectMessage = getResources().getString(R.string.Bluetooth_Disconnect_Disconnect_Message);

        new AlertDialog.Builder(getActivity())
            .setTitle(disconnectTitle)
            .setMessage(disconnectMessage)

            .setPositiveButton(R.string.General_Notification_Button_Ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                    disconnectProgressBluetooth = ProgressDialog.show(getActivity(), getString(R.string.Bluetooth_Disconnect_Disconnect_Progress_Title), getString(R.string.Bluetooth_Disconnect_Disconnect_Progress_Message), true);

                    disconnectPressed = true;

                    BluetoothService.stopClient();
                }
            })

            .setNegativeButton(R.string.General_Notification_Button_Cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {

                }
            })

            .show();
    }

    private void addWifi() {
        name = new EditText(getActivity());
        name.setHint(R.string.Wifi_Name_Hint);
        name.setSingleLine(true);
        name.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(25) });
        name.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        name.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.DarkGreen));

        address = new EditText(getActivity());
        address.setHint(R.string.Wifi_Address_Hint);
        address.setSingleLine(true);
        address.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(15) });
        address.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        address.setKeyListener(DigitsKeyListener.getInstance("0123456789."));
        address.setTextColor(ContextCompat.getColor(getContext(), R.color.DarkGreen));

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        layout.setPadding(60, 0, 60, 0);
        layout.addView(name);
        layout.addView(address);

        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.Wifi_Add_Connection_Title)
                .setMessage(R.string.Wifi_Add_Connection_Message)
                .setView(layout)

                .setPositiveButton(R.string.General_Notification_Button_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (!name.getText().toString().isEmpty()) {
                            if (!address.getText().toString().isEmpty()) {
                                Pattern IP_ADDRESS = Pattern.compile(
                                        "((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
                                                + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
                                                + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
                                                + "|[1-9][0-9]|[0-9]))");


                                Matcher matcher = IP_ADDRESS.matcher(address.getText().toString());

                                if (matcher.matches()) {
                                    entries.append(name.getText().toString()).append("\n").append(address.getText().toString()).append(",");

                                    SharedPreferences pref = Objects.requireNonNull(getActivity()).getSharedPreferences("prefs", Activity.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = pref.edit();

                                    editor.putString("wifiList", entries.toString());
                                    editor.apply();

                                    String[] splitEntries = entries.toString().split(",");

                                    deviceList.clear();

                                    Collections.addAll(deviceList, splitEntries);

                                    wifiDeviceList.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, deviceList));

                                    if (!deviceList.isEmpty()) {
                                        WifiHeader.setVisibility(View.VISIBLE);
                                    } else {
                                        WifiHeader.setVisibility(View.GONE);
                                    }
                                } else {
                                    Snackbar.make(view, R.string.Wifi_Connect_Wrong_IP, Snackbar.LENGTH_SHORT).show();
                                }
                            } else {
                                Snackbar.make(view, R.string.Wifi_Add_Connection_No_Address, Snackbar.LENGTH_SHORT).show();
                            }
                        } else {
                            Snackbar.make(view, R.string.Wifi_Add_Connection_No_Name, Snackbar.LENGTH_SHORT).show();
                        }
                    }
                })

                .setNegativeButton(R.string.General_Notification_Button_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                })

                .show();
    }

    private void connectWiFi(int position) {
        if (!MainActivity.wifiConnected) {
            String name = wifiDeviceList.getItemAtPosition(position).toString();
            String[] separated = name.split("\n");

            String ipAddress = separated[1];

            connectProgressWiFi = ProgressDialog.show(getActivity(), getString(R.string.Wifi_Connect_Connect_Progress_Title), getString(R.string.Wifi_Connect_Connect_Progress_Message), true);
            connectPressedWiFi = true;

            Intent intent = new Intent(getActivity(), WiFiService.class);
            intent.putExtra(MainActivity.START_FLAG, "Start");
            intent.putExtra(MainActivity.IP_ADDRESS, ipAddress);
            Objects.requireNonNull(getActivity()).startService(intent);
        } else {
            Snackbar.make(view, R.string.Wifi_Connect_Already_Connected, Snackbar.LENGTH_SHORT).show();
        }
    }

    private void disconnectWiFi() {
        String disconnectTitle = getResources().getString(R.string.Wifi_Disconnect_Disconnect_Title);
        String disconnectMessage = getResources().getString(R.string.Wifi_Disconnect_Disconnect_Message);

        new AlertDialog.Builder(getActivity())
                .setTitle(disconnectTitle)
                .setMessage(disconnectMessage)

                .setPositiveButton(R.string.General_Notification_Button_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        disconnectProgressWiFi = ProgressDialog.show(getActivity(), getString(R.string.Wifi_Disconnect_Disconnect_Progress_Title), getString(R.string.Wifi_Disconnect_Disconnect_Progress_Message), true);
                        disconnectPressed = true;

                        Intent intent = new Intent(MainActivity.NOTIFICATION);
                        intent.putExtra(MainActivity.DATA, "DisconnectWiFi");
                        Objects.requireNonNull(getActivity()).sendBroadcast(intent);

                        WiFiService.stopClient();
                    }
                })

                .setNegativeButton(R.string.General_Notification_Button_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                })

                .show();
    }

    public static void showSnackbar(int message, int duration) {
        Snackbar.make(view, message, duration).show();
    }
}