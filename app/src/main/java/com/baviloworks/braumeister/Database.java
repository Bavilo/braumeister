/*
 * Copyright (C) 2016 Braumeister by BaviloWorks <BaviloWorks@gmail.com>
 *
 * This work is licensed under the Creative Commons
 * Attribution-NonCommercial-ShareAlike 4.0 International License.
 *
 * You may not use this file except in compliance with the License.
 * To view a copy of this license, visit
 *
 *      http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baviloworks.braumeister;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.EditText;
import android.widget.Spinner;

class Database extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "braumeister.db";
    private static final String TABLE_NAME = "recipes";
    private static final String COL_1 = "_id";
    public static final String COL_2 = "RECIPE_NAME";
    private static final String COL_3 = "AMOUNT_OF_RESTS";
    private static final String COL_4 = "REST_SELECTION_1";
    private static final String COL_5 = "REST_SELECTION_2";
    private static final String COL_6 = "REST_SELECTION_3";
    private static final String COL_7 = "REST_SELECTION_4";
    private static final String COL_8 = "REST_SELECTION_5";
    private static final String COL_9 = "REST_SELECTION_6";
    private static final String COL_10 = "REST_SELECTION_7";
    private static final String COL_11 = "MASHING_TEMPERATURE";
    private static final String COL_12 = "REST_TEMPERATURE_1";
    private static final String COL_13 = "REST_TIME_1";
    private static final String COL_14 = "REST_TEMPERATURE_2";
    private static final String COL_15 = "REST_TIME_2";
    private static final String COL_16 = "REST_TEMPERATURE_3";
    private static final String COL_17 = "REST_TIME_3";
    private static final String COL_18 = "REST_TEMPERATURE_4";
    private static final String COL_19 = "REST_TIME_4";
    private static final String COL_20 = "REST_TEMPERATURE_5";
    private static final String COL_21 = "REST_TIME_5";
    private static final String COL_22 = "REST_TEMPERATURE_6";
    private static final String COL_23 = "REST_TIME_6";
    private static final String COL_24 = "REST_TEMPERATURE_7";
    private static final String COL_25 = "REST_TIME_7";
    private static final String COL_26 = "REST_COMMENT_1";
    private static final String COL_27 = "REST_COMMENT_2";
    private static final String COL_28 = "REST_COMMENT_3";
    private static final String COL_29 = "REST_COMMENT_4";
    private static final String COL_30 = "REST_COMMENT_5";
    private static final String COL_31 = "REST_COMMENT_6";
    private static final String COL_32 = "REST_COMMENT_7";
    private static final String COL_33 = "SPARGE_AMOUNT";
    private static final String COL_34 = "AMOUNT_OF_HOPPINGS";
    private static final String COL_35 = "TOTAL_COOKING_DURATION";
    private static final String COL_36 = "COOKING_DURATION_1";
    private static final String COL_37 = "COOKING_DURATION_2";
    private static final String COL_38 = "COOKING_DURATION_3";
    private static final String COL_39 = "COOKING_DURATION_4";
    private static final String COL_40 = "COOKING_DURATION_5";
    private static final String COL_41 = "COOKING_DURATION_6";
    private static final String COL_42 = "COOKING_DURATION_7";
    private static final String COL_43 = "HOPPING_COMMENT_1";
    private static final String COL_44 = "HOPPING_COMMENT_2";
    private static final String COL_45 = "HOPPING_COMMENT_3";
    private static final String COL_46 = "HOPPING_COMMENT_4";
    private static final String COL_47 = "HOPPING_COMMENT_5";
    private static final String COL_48 = "HOPPING_COMMENT_6";
    private static final String COL_49 = "HOPPING_COMMENT_7";
    private static final String COL_50 = "IS_VWH";

    Database(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS "
                + TABLE_NAME + " (" + COL_1 + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_2 + " VARCHAR(255) UNIQUE, "
                + COL_3 + " INTEGER, " + COL_4 + " INTEGER, " + COL_5 + " INTEGER, " + COL_6 + " INTEGER, "
                + COL_7 + " INTEGER, " + COL_8 + " INTEGER, " + COL_9 + " INTEGER, " + COL_10 + " INTEGER, "
                + COL_11 + " INTEGER, " + COL_12 + " INTEGER, " + COL_13 + " INTEGER, " + COL_14 + " INTEGER, "
                + COL_15 + " INTEGER, " + COL_16 + " INTEGER, " + COL_17 + " INTEGER, " + COL_18 + " INTEGER, "
                + COL_19 + " INTEGER, " + COL_20 + " INTEGER, " + COL_21 + " INTEGER, " + COL_22 + " INTEGER, "
                + COL_23 + " INTEGER, " + COL_24 + " INTEGER, " + COL_25 + " INTEGER, "
                + COL_26 + " TEXT, " + COL_27 + " TEXT, " + COL_28 + " TEXT, " + COL_29 + " TEXT, "
                + COL_30 + " TEXT, " + COL_31 + " TEXT, " + COL_32 + " TEXT, "
                + COL_33 + " INTEGER, " + COL_34 + " INTEGER, " + COL_35 + " INTEGER, " + COL_36 + " INTEGER, " + COL_37 + " INTEGER, "
                + COL_38 + " INTEGER, " + COL_39 + " INTEGER, " + COL_40 + " INTEGER, " + COL_41 + " INTEGER, "
                + COL_42 + " INTEGER, " + COL_43 + " TEXT, " + COL_44 + " TEXT, " + COL_45 + " TEXT, " + COL_46 + " TEXT, "
                + COL_47 + " TEXT, " + COL_48 + " TEXT, " + COL_49 + " TEXT, " + COL_50 + " TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    void insertEmptyData(String recipeName) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_2, recipeName);
        contentValues.put(COL_3, 0);
        contentValues.put(COL_4, 0);
        contentValues.put(COL_5, 0);
        contentValues.put(COL_6, 0);
        contentValues.put(COL_7, 0);
        contentValues.put(COL_8, 0);
        contentValues.put(COL_9, 0);
        contentValues.put(COL_10, 0);
        contentValues.put(COL_34, 0);
        contentValues.put(COL_50, "false");

        db.insert(TABLE_NAME, null, contentValues);
    }

    void insertFullData(String recipeName, int amountOfRests, int mashingTemperature, int[] restSelection, int[] restTemperature, int[] restTime, float spargeAmount, int amountOfHoppings, int cookingTimeTotal, int[] cookingTime, String[] hoppingComment, String isVWH) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_2, recipeName);
        contentValues.put(COL_3, amountOfRests);
        contentValues.put(COL_4, restSelection[0]);
        contentValues.put(COL_5, restSelection[1]);
        contentValues.put(COL_6, restSelection[2]);
        contentValues.put(COL_7, restSelection[3]);
        contentValues.put(COL_8, restSelection[4]);
        contentValues.put(COL_9, restSelection[5]);
        contentValues.put(COL_10, restSelection[6]);
        contentValues.put(COL_11, mashingTemperature);
        contentValues.put(COL_12, restTemperature[0]);
        contentValues.put(COL_13, restTime[0]);
        contentValues.put(COL_14, restTemperature[1]);
        contentValues.put(COL_15, restTime[1]);
        contentValues.put(COL_16, restTemperature[2]);
        contentValues.put(COL_17, restTime[2]);
        contentValues.put(COL_18, restTemperature[3]);
        contentValues.put(COL_19, restTime[3]);
        contentValues.put(COL_20, restTemperature[4]);
        contentValues.put(COL_21, restTime[4]);
        contentValues.put(COL_22, restTemperature[5]);
        contentValues.put(COL_23, restTime[5]);
        contentValues.put(COL_24, restTemperature[6]);
        contentValues.put(COL_25, restTime[6]);
        contentValues.put(COL_26, "");
        contentValues.put(COL_27, "");
        contentValues.put(COL_28, "");
        contentValues.put(COL_29, "");
        contentValues.put(COL_30, "");
        contentValues.put(COL_31, "");
        contentValues.put(COL_32, "");
        contentValues.put(COL_33, spargeAmount);
        contentValues.put(COL_34, amountOfHoppings);
        contentValues.put(COL_35, cookingTimeTotal);
        contentValues.put(COL_36, cookingTime[0]);
        contentValues.put(COL_37, cookingTime[1]);
        contentValues.put(COL_38, cookingTime[2]);
        contentValues.put(COL_39, cookingTime[3]);
        contentValues.put(COL_40, cookingTime[4]);
        contentValues.put(COL_41, cookingTime[5]);
        contentValues.put(COL_42, cookingTime[6]);
        contentValues.put(COL_43, hoppingComment[0]);
        contentValues.put(COL_44, hoppingComment[1]);
        contentValues.put(COL_45, hoppingComment[2]);
        contentValues.put(COL_46, hoppingComment[3]);
        contentValues.put(COL_47, hoppingComment[4]);
        contentValues.put(COL_48, hoppingComment[5]);
        contentValues.put(COL_49, hoppingComment[6]);
        contentValues.put(COL_50, isVWH);

        db.insert(TABLE_NAME, null, contentValues);
    }

    boolean updateMashingData(String recipeName, int amountOfRests, EditText mashingTemperature, Spinner[] restSelection, EditText[] restTemperature, EditText[] restTime, EditText[] restComment) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_3, amountOfRests);
        contentValues.put(COL_4, restSelection[0].getSelectedItemPosition());
        contentValues.put(COL_5, restSelection[1].getSelectedItemPosition());
        contentValues.put(COL_6, restSelection[2].getSelectedItemPosition());
        contentValues.put(COL_7, restSelection[3].getSelectedItemPosition());
        contentValues.put(COL_8, restSelection[4].getSelectedItemPosition());
        contentValues.put(COL_9, restSelection[5].getSelectedItemPosition());
        contentValues.put(COL_10, restSelection[6].getSelectedItemPosition());
        contentValues.put(COL_11, mashingTemperature.getText().toString());
        contentValues.put(COL_12, restTemperature[0].getText().toString());
        contentValues.put(COL_13, restTime[0].getText().toString());
        contentValues.put(COL_14, restTemperature[1].getText().toString());
        contentValues.put(COL_15, restTime[1].getText().toString());
        contentValues.put(COL_16, restTemperature[2].getText().toString());
        contentValues.put(COL_17, restTime[2].getText().toString());
        contentValues.put(COL_18, restTemperature[3].getText().toString());
        contentValues.put(COL_19, restTime[3].getText().toString());
        contentValues.put(COL_20, restTemperature[4].getText().toString());
        contentValues.put(COL_21, restTime[4].getText().toString());
        contentValues.put(COL_22, restTemperature[5].getText().toString());
        contentValues.put(COL_23, restTime[5].getText().toString());
        contentValues.put(COL_24, restTemperature[6].getText().toString());
        contentValues.put(COL_25, restTime[6].getText().toString());
        contentValues.put(COL_26, restComment[0].getText().toString());
        contentValues.put(COL_27, restComment[1].getText().toString());
        contentValues.put(COL_28, restComment[2].getText().toString());
        contentValues.put(COL_29, restComment[3].getText().toString());
        contentValues.put(COL_30, restComment[4].getText().toString());
        contentValues.put(COL_31, restComment[5].getText().toString());
        contentValues.put(COL_32, restComment[6].getText().toString());


        db.update(TABLE_NAME, contentValues, "RECIPE_NAME = ?", new String[]{ recipeName });

        return true;
    }

    boolean updateLauteringData(String recipeName, EditText spargeAmount) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_33, spargeAmount.getText().toString());

        db.update(TABLE_NAME, contentValues, "RECIPE_NAME = ?", new String[]{ recipeName });

        return true;
    }

    boolean updateCookingData(String recipeName, int amountOfHoppings, EditText cookingTimeTotal, EditText[] cookingTime, EditText[] hoppingComment, String isVWH) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_34, amountOfHoppings);
        contentValues.put(COL_35, cookingTimeTotal.getText().toString());
        contentValues.put(COL_36, cookingTime[0].getText().toString());
        contentValues.put(COL_37, cookingTime[1].getText().toString());
        contentValues.put(COL_38, cookingTime[2].getText().toString());
        contentValues.put(COL_39, cookingTime[3].getText().toString());
        contentValues.put(COL_40, cookingTime[4].getText().toString());
        contentValues.put(COL_41, cookingTime[5].getText().toString());
        contentValues.put(COL_42, cookingTime[6].getText().toString());
        contentValues.put(COL_43, hoppingComment[0].getText().toString());
        contentValues.put(COL_44, hoppingComment[1].getText().toString());
        contentValues.put(COL_45, hoppingComment[2].getText().toString());
        contentValues.put(COL_46, hoppingComment[3].getText().toString());
        contentValues.put(COL_47, hoppingComment[4].getText().toString());
        contentValues.put(COL_48, hoppingComment[5].getText().toString());
        contentValues.put(COL_49, hoppingComment[6].getText().toString());
        contentValues.put(COL_50, isVWH);

        db.update(TABLE_NAME, contentValues, "RECIPE_NAME = ?", new String[] { recipeName });

        return true;
    }

    boolean checkIfDataExists(String name) {
        SQLiteDatabase db = this.getWritableDatabase();

        int count = -1;

        String query = "SELECT COUNT(*) FROM " + TABLE_NAME + " WHERE RECIPE_NAME = ? COLLATE NOCASE";
        Cursor cursor = db.rawQuery(query, new String[] { name });

        if (cursor.moveToFirst()) {
            count = cursor.getInt(0);
        }

        if (count > 0) {
            cursor.close();

            return false;
        } else {
            cursor.close();

            return true;
        }
    }

    Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }

    void deleteData(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, "RECIPE_NAME = ?", new String[]{ name });
    }
}