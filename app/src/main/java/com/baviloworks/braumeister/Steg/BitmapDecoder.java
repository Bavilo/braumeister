/*
 * Copyright (C) 2016 Braumeister by BaviloWorks <BaviloWorks@gmail.com>
 *
 * This work is licensed under the Creative Commons
 * Attribution-NonCommercial-ShareAlike 4.0 International License.
 *
 * You may not use this file except in compliance with the License.
 * To view a copy of this license, visit
 *
 *      http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baviloworks.braumeister.Steg;

import android.graphics.Bitmap;
import android.graphics.Color;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class BitmapDecoder {
    private static final int HEADER_SIZE = Long.SIZE / Byte.SIZE + 4;

    private static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.SIZE / Byte.SIZE);
        buffer.put(bytes);
        buffer.flip();

        return buffer.getLong();
    }

    public static byte[] decode(Bitmap inBitmap) {
        byte[] header = decodeBitmapToByteArray(inBitmap, 0, HEADER_SIZE);
        int len = (int) bytesToLong(Arrays.copyOfRange(header, 2, HEADER_SIZE - 2));

        return decodeBitmapToByteArray(inBitmap, HEADER_SIZE, len);
    }

    private static byte[] decodeBitmapToByteArray(Bitmap inBitmap, int offset, int length) {
        byte bytes[] = new byte[length];

        int width = inBitmap.getWidth();
        int height = inBitmap.getHeight();

        int color;

        int bitNo = 0;
        int byteNo = 0;

        int bitBuffer[] = new int[3];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                color = inBitmap.getPixel(x, y);

                bitBuffer[0] = Color.red(color) % 2;
                bitBuffer[1] = Color.green(color) % 2;
                bitBuffer[2] = Color.blue(color) % 2;

                for (int i = 0; i < 3; i++) {
                    if (byteNo >= offset) {
                        bytes[byteNo - offset] = bitBuffer[i] == 1 ? (byte) (bytes[byteNo - offset] | (1 << bitNo)) : (byte) (bytes[byteNo - offset] & ~(1 << bitNo));
                    }

                    bitNo++;

                    if (bitNo == 8) {
                        bitNo = 0;

                        byteNo++;
                    }

                    if (byteNo - offset >= bytes.length) break;
                }

                if (byteNo - offset >= bytes.length) break;
            }

            if (byteNo - offset >= bytes.length) break;
        }

        return bytes;
    }
}
