/*
 * Copyright (C) 2016 Braumeister by BaviloWorks <BaviloWorks@gmail.com>
 *
 * This work is licensed under the Creative Commons
 * Attribution-NonCommercial-ShareAlike 4.0 International License.
 *
 * You may not use this file except in compliance with the License.
 * To view a copy of this license, visit
 *
 *      http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baviloworks.braumeister;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.baviloworks.braumeister.ConnectionServices.BluetoothService;
import com.baviloworks.braumeister.ConnectionServices.WiFiService;

public class SettingsActivity extends AppCompatActivity {
    private View parentLayout;

    private TextView TemperatureSensorHG, TemperatureSensorNG, LgHysteresis, LbHysteresis, LgPumpTime, LbPumpTime;
    private EditText newLgHysteresis, newLbHysteresis, newLgPumpTime, newLbPumpTime;
    private EditText emailAddress;
    private Switch emailSwitch;
    private Button emailButton;

    public static String stringTemperatureSensorHG = "";
    public static String stringTemperatureSensorNG = "";
    public static String stringLgHysteresis = "";
    public static String stringLbHysteresis = "";
    public static String stringLgPumpTime = "";
    public static String stringLbPumpTime = "";

    public static String emailRecipient = "";

    public static boolean emailNotification = false;
    private boolean threadLoop = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.settings);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setTaskDescription(new ActivityManager.TaskDescription(null, null, ContextCompat.getColor(getApplicationContext(), R.color.Blue)));
        }

        Toolbar toolbar = findViewById(R.id.Toolbar);

        if (toolbar != null) {
            toolbar.setTitle(getString(R.string.Main_Menu_Settings_Title));
            setSupportActionBar(toolbar);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        parentLayout = findViewById(android.R.id.content);

        TemperatureSensorHG = findViewById(R.id.temperatureSensorHG);
        TemperatureSensorNG = findViewById(R.id.temperatureSensorNG);
        LgHysteresis = findViewById(R.id.LgHysteresis);
        LbHysteresis = findViewById(R.id.LbHysteresis);
        LgPumpTime = findViewById(R.id.lgPumpTime);
        LbPumpTime = findViewById(R.id.lbPumpTime);

        newLgHysteresis = findViewById(R.id.newLgHysteresis);
        newLbHysteresis = findViewById(R.id.newLbHysteresis);
        newLgPumpTime = findViewById(R.id.newLgPumpTime);
        newLbPumpTime = findViewById(R.id.newLbPumpTime);
        emailAddress = findViewById(R.id.emailAddress);

        emailButton = findViewById(R.id.emailButton);

        emailSwitch = findViewById(R.id.emailSwitch);

        if (emailNotification && !emailRecipient.equals("")) {
            emailSwitch.setChecked(true);
            emailAddress.setEnabled(true);
            emailAddress.setText(emailRecipient);
            emailButton.setEnabled(true);
        } else {
            emailNotification = false;

            emailSwitch.setChecked(false);
            emailAddress.setEnabled(false);
            emailButton.setEnabled(false);
        }

        newLgHysteresis.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable != null && editable.length() == 5) {
                    editable.delete(0, 5);
                } else {
                    if (!newLgHysteresis.getText().toString().equals("")) {
                        if (Float.parseFloat(newLgHysteresis.getText().toString()) > 25.4) {
                            newLgHysteresis.setText("25.4");
                        }
                    }
                }
            }
        });

        newLbHysteresis.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable != null && editable.length() == 5) {
                    editable.delete(0, 5);
                } else {
                    if (!newLbHysteresis.getText().toString().equals("")) {
                        if (Float.parseFloat(newLbHysteresis.getText().toString()) > 25.4) {
                            newLbHysteresis.setText("25.4");
                        }
                    }
                }
            }
        });

        newLgPumpTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable != null && editable.length() == 4) {
                    editable.delete(0, 4);
                } else {
                    if (!newLgPumpTime.getText().toString().equals("")) {
                        if (Integer.parseInt(newLgPumpTime.getText().toString()) > 254) {
                            newLgPumpTime.setText("254");
                        }
                    }
                }
            }
        });

        newLbPumpTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable != null && editable.length() == 4) {
                    editable.delete(0, 4);
                } else {
                    if (!newLbPumpTime.getText().toString().equals("")) {
                        if (Integer.parseInt(newLbPumpTime.getText().toString()) > 254) {
                            newLbPumpTime.setText("254");
                        }
                    }
                }
            }
        });

        emailSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferences sp = getSharedPreferences("prefs", Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();

                if (b) {
                    emailNotification = true;

                    emailAddress.setEnabled(true);
                    emailButton.setEnabled(true);
                } else {
                    emailNotification = false;

                    emailRecipient = "";

                    emailAddress.setText("");
                    emailAddress.setEnabled(false);
                    emailButton.setEnabled(false);
                    emailButton.setVisibility(View.GONE);

                    editor.putString("emailAddress", emailRecipient);
                }

                editor.putBoolean("emailSwitch", emailNotification);
                editor.apply();
            }
        });

        emailAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                emailButton.setVisibility(View.VISIBLE);
            }
        });

        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sp = getSharedPreferences("prefs", Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();

                emailRecipient = emailAddress.getText().toString();

                if (android.util.Patterns.EMAIL_ADDRESS.matcher(emailRecipient).matches()) {
                    Snackbar.make(parentLayout, R.string.Settings_Email_Saved, Snackbar.LENGTH_SHORT).show();
                } else {
                    emailRecipient = "";
                    emailAddress.setText("");

                    Snackbar.make(parentLayout, R.string.Settings_Invalid_Email, Snackbar.LENGTH_SHORT).show();
                }

                emailButton.setVisibility(View.GONE);

                editor.putString("emailAddress", emailRecipient);
                editor.apply();
            }
        });

        threadLoop = true;

        new Thread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

                while (threadLoop) {
                    updateSettings();

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Log.e("Exception", "Could not put thread to sleep - settingsUpdate");
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.Update:
                update();
                break;
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem update = menu.findItem(R.id.Update);

        if (MainActivity.hideSettingsItems) {
            update.setEnabled(false);
            update.getIcon().setAlpha(128);
        } else {
            update.setEnabled(true);
            update.getIcon().setAlpha(255);
        }

        invalidateOptionsMenu();

        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        threadLoop = false;
    }

    private void update() {
        if (MainActivity.bluetoothConnected || MainActivity.wifiConnected) {
            if (newLgHysteresis.getText().toString().equals("")) {
                newLgHysteresis.setText(stringLgHysteresis);
            }

            if (newLbHysteresis.getText().toString().equals("")) {
                newLbHysteresis.setText(stringLbHysteresis);
            }

            if (newLgPumpTime.getText().toString().equals("")) {
                newLgPumpTime.setText(stringLgPumpTime);
            }

            if (newLbPumpTime.getText().toString().equals("")) {
                newLbPumpTime.setText(stringLbPumpTime);
            }

            if (MainActivity.bluetoothConnected) {
                BluetoothService.sendMessage(
                        "E" + newLgHysteresis.getText().toString() + ","
                                + newLbHysteresis.getText().toString() + ","
                                + newLgPumpTime.getText().toString() + ","
                                + newLbPumpTime.getText().toString() + "!"
                );
            } else {
                WiFiService.sendMessage(
                        "E" + newLgHysteresis.getText().toString() + ","
                                + newLbHysteresis.getText().toString() + ","
                                + newLgPumpTime.getText().toString() + ","
                                + newLbPumpTime.getText().toString() + "!"
                );
            }

            newLgHysteresis.setText("");
            newLbHysteresis.setText("");
            newLgPumpTime.setText("");
            newLbPumpTime.setText("");

            Snackbar.make(parentLayout, R.string.Settings_Saved, Snackbar.LENGTH_SHORT).show();
        } else {
            Snackbar.make(parentLayout, R.string.Bluetooth_Disconnect_Not_Connected, Snackbar.LENGTH_SHORT).show();
        }
    }

    private void updateSettings() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

                switch (stringTemperatureSensorHG) {
                    case "-127.00":
                        TemperatureSensorHG.setText(getString(R.string.Settings_Sensor_Fault));
                        break;
                    case "":
                        TemperatureSensorHG.setText(getString(R.string.Settings_NA));
                        break;
                    default:
                        TemperatureSensorHG.setText(stringTemperatureSensorHG + getString(R.string.General_Celcius));
                        break;
                }

                switch (stringTemperatureSensorNG) {
                    case "-127.00":
                        TemperatureSensorNG.setText(getString(R.string.Settings_Sensor_Fault));
                        break;
                    case "":
                        TemperatureSensorNG.setText(getString(R.string.Settings_NA));
                        break;
                    default:
                        TemperatureSensorNG.setText(stringTemperatureSensorNG + getString(R.string.General_Celcius));
                        break;
                }

                if (stringLgHysteresis.equals("")) {
                    LgHysteresis.setText(getString(R.string.Settings_NA));
                } else {
                    LgHysteresis.setText(stringLgHysteresis + getString(R.string.General_Celcius));
                }

                if (stringLbHysteresis.equals("")) {
                    LbHysteresis.setText(getString(R.string.Settings_NA));
                } else {
                    LbHysteresis.setText(stringLbHysteresis + getString(R.string.General_Celcius));
                }

                if (stringLgPumpTime.equals("")) {
                    LgPumpTime.setText(getString(R.string.Settings_NA));
                } else {
                    LgPumpTime.setText(stringLgPumpTime + " " + getString(R.string.General_Seconds));
                }

                if (stringLbPumpTime.equals("")) {
                    LbPumpTime.setText(getString(R.string.Settings_NA));
                } else {
                    LbPumpTime.setText(stringLbPumpTime + " " + getString(R.string.General_Seconds));
                }
            }
        });
    }
}