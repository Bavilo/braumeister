/*
 * Copyright (C) 2016 Braumeister by BaviloWorks <BaviloWorks@gmail.com>
 *
 * This work is licensed under the Creative Commons
 * Attribution-NonCommercial-ShareAlike 4.0 International License.
 *
 * You may not use this file except in compliance with the License.
 * To view a copy of this license, visit
 *
 *      http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baviloworks.braumeister.ConnectionServices;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.baviloworks.braumeister.Connection;
import com.baviloworks.braumeister.Email.GMailSender;
import com.baviloworks.braumeister.MainActivity;
import com.baviloworks.braumeister.R;
import com.baviloworks.braumeister.SettingsActivity;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.UUID;

public class BluetoothService extends Service {
    private static BluetoothSocket mSocket;
    private static PrintWriter mBufferOut;
    private static BufferedReader mBufferIn;

    private static boolean mRun = false;

    private final UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        new Thread(new Runnable() {
            public void run() {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_FOREGROUND);

                Bundle bundle = intent.getExtras();

                if (bundle != null) {
                    String action = bundle.getString(MainActivity.START_FLAG);
                    String mac = bundle.getString(MainActivity.MAC_ADDRESS);

                    if (action != null && action.equals("Start")) {
                        mRun = true;

                        try {
                            BluetoothDevice device = MainActivity.bluetoothAdapter.getRemoteDevice(mac);

                            mSocket = device.createRfcommSocketToServiceRecord(SPP_UUID);
                            mSocket.connect();

                            try {
                                mBufferOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(mSocket.getOutputStream())), true);
                                mBufferIn = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));

                                while (mRun) {
                                    if (Connection.connectPressedBluetooth) {
                                        sendMessage("?!");

                                        Connection.connectPressedBluetooth = false;
                                    }

                                    String readMessage = mBufferIn.readLine();

                                    if (readMessage != null) {
                                        publishResults(readMessage);
                                    }
                                }
                            } catch (Exception e) {
                                if (!Connection.disconnectPressed && !Connection.connectPressedBluetooth) {
                                    showNotification(MainActivity.disconnectID, getString(R.string.Main_Bluetooth_Disconnected_Notification_Title), getString(R.string.Main_Bluetooth_Disconnected_Notification_Message));
                                    sendEmail(SettingsActivity.emailRecipient, getString(R.string.Main_Bluetooth_Disconnected_Notification_Title), getString(R.string.Main_Bluetooth_Disconnected_Notification_Message));
                                }

                                stopClient();

                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            if (Connection.connectionIsAlive) {
                                Connection.showSnackbar(R.string.Main_Bluetooth_SPP_Only, Snackbar.LENGTH_SHORT);
                            }

                            if (Connection.disconnectProgressBluetooth != null && Connection.disconnectProgressBluetooth.isShowing()) {
                                Connection.disconnectProgressBluetooth.dismiss();
                            }

                            if (Connection.connectProgressBluetooth != null && Connection.connectProgressBluetooth.isShowing()) {
                                Connection.connectProgressBluetooth.dismiss();
                            }

                            stopClient();

                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();

        return Service.START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static void sendMessage(String input) {
        if (mBufferOut != null && !mBufferOut.checkError()) {
            mBufferOut.println(input);
            mBufferOut.flush();
        }
    }

    public static void stopClient() {
        mRun = false;

        if (mBufferOut != null) {
            mBufferOut.flush();
            mBufferOut.close();
        }

        mBufferIn = null;
        mBufferOut = null;

        try {
            mSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void publishResults(String data) {
        Intent intent = new Intent(MainActivity.NOTIFICATION);
        intent.putExtra(MainActivity.DATA, data);
        sendBroadcast(intent);
    }

    private void showNotification(int ID, String title, String message) {
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_notification_logo);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent resultIntent = new Intent(this, MainActivity.class);

        switch (ID) {
            case 0:
                resultIntent.setAction(MainActivity.connectionAction);
                break;
            case 1:
                resultIntent.setAction(MainActivity.mashingAction);
                break;
            case 2:
                resultIntent.setAction(MainActivity.cookingAction);
                break;
            case 3:
                resultIntent.setAction(MainActivity.hoppingAction);
                break;
            case 4:
                resultIntent.setAction(MainActivity.spargingAction);
                break;
            case 5:
                resultIntent.setAction(MainActivity.sensorAction);
                break;
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "M_CH_ID")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(getApplicationContext(), R.color.NotificationBackground))
                .setSmallIcon(R.drawable.ic_announcement_white_24dp)
                .setLargeIcon(largeIcon)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setLights(ContextCompat.getColor(getApplicationContext(), R.color.Blue), 300, 1000)
                .setSound(soundUri)
                .setContentIntent(PendingIntent.getActivity(this, 0, resultIntent, 0));

        NotificationManager notifyMgr = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        assert notifyMgr != null;
        notifyMgr.notify(ID, builder.build());
    }

    private void sendEmail(final String recipient, final String subject, final String body) {
        new Thread(new Runnable() {
            public void run() {
                if (SettingsActivity.emailNotification && !recipient.equals("")) {
                    if (!MainActivity.file.equals("")) {
                        try {
                            GMailSender sender = new GMailSender();

                            sender.sendMail(subject, body, recipient);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        showNotification(MainActivity.emailID, getString(R.string.Main_Email_No_Password_Title), getString(R.string.Main_Email_No_Password_Message));
                    }
                }
            }
        }).start();
    }
}
