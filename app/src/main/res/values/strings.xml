<?xml version="1.0" encoding="utf-8"?>
<!--
   Copyright (C) 2016 Braumeister by BaviloWorks <BaviloWorks@gmail.com>

   This work is licensed under the Creative Commons
   Attribution-NonCommercial-ShareAlike 4.0 International License.

   You may not use this file except in compliance with the License.
   To view a copy of this license, visit

        http://creativecommons.org/licenses/by-nc-sa/4.0/

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
-->

<resources>
    <string name="APP_Name">Braumeister</string>
    <string name="Main_Name">Braumeister</string>
    <string name="Settings_Name">Settings</string>
    <string name="Debugging_Name">Debugging</string>

    <string name="General_Notification_Button_Save">Save</string>
    <string name="General_Notification_Button_Cancel">Cancel</string>
    <string name="General_Notification_Button_Ok">OK</string>
    <string name="General_Liter">Liter</string>
    <string name="General_Celcius">°C</string>
    <string name="General_Minutes">Min</string>
    <string name="General_Seconds">Sec</string>
    <string name="General_hms">h:mm:ss</string>

    <string name="Main_Tab_Connection">Connections</string>
    <string name="Main_Tab_Recipe">Recipe</string>
    <string name="Main_Tab_Mashing">Mashing</string>
    <string name="Main_Tab_Sparging">Sparging</string>
    <string name="Main_Tab_Cooking">Cooking</string>
    <string name="Main_Menu_Settings">Settings</string>
    <string name="Main_Menu_Debugging">Debugging</string>
    <string name="Main_Menu_Share">Share recipe</string>
    <string name="Main_Menu_Mainmenu">Menu</string>
    <string name="Main_Menu_Info">About the app</string>
    <string name="Main_Menu_Settings_Title">Settings</string>
    <string name="Main_Menu_Debugging_Title">Debugging</string>
    <string name="Main_Menu_Info_Title">About the app</string>
    <string name="Main_Menu_Info_Message">
         <![CDATA[
            Version: 1.5.2<br /><br />
            Copyright &#169; 2016 Braumeister by BaviloWorks (Michael Weißenberger)<br /><br /><a href="mailto:BaviloWorks@gmail.com">BaviloWorks@gmail.com</a><br /><br />

            This work is licensed under the Creative Commons<br />
            Attribution-NonCommercial-ShareAlike 4.0 International License.<br /><br />

            You may not use this file except in compliance with the License.<br />
            To view a copy of this license, visit<br /><br />

            <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">http://creativecommons.org/licenses/by-nc-sa/4.0/</a><br /><br />

            Unless required by applicable law or agreed to in writing, software<br />
            distributed under the License is distributed on an "AS IS" BASIS,<br />
            WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.<br />
            See the License for the specific language governing permissions and<br />
            limitations under the License.
        ]]>
    </string>
    <string name="Main_Alert_Title_Exit">Exit</string>
    <string name="Main_Alert_Message_Exit">Do you really wish to exit Braumeister?</string>
    <string name="Main_Bluetooth_Not_Supported">This device does not support Bluetooth</string>
    <string name="Main_Bluetooth_SPP_Only">A connection could not be established. (SPP only)</string>
    <string name="Main_Bluetooth_No_Device_Found">No Bluetooth devices found</string>
    <string name="Main_Bluetooth_Disconnected_Notification_Title">You\'ve been disconnected</string>
    <string name="Main_Bluetooth_Disconnected_Notification_Message">When you reconnect to the controller, you can keep monitoring the current progress</string>
    <string name="Main_Need_Write_External_Storage_Permission_Title">Storage access required</string>
    <string name="Main_Need_Write_External_Storage_Permission_Message">In order to load recipe files, you need to grant this app storage access</string>
    <string name="Main_Limited_Functionality_Title">Limited functionality</string>
    <string name="Main_Limited_Functionality_Message">Since you have denied the permission, the app can not access the storage</string>
    <string name="Main_Install_File_Manager">Please install a file manager</string>
    <string name="Main_Not_Valid_Extension">This is not a valid KBH recipe</string>
    <string name="Main_Share_Send_To">Send to…</string>
    <string name="Main_Share_Share_Recipe_Title">Share recipe</string>
    <string name="Main_Share_Share_Recipe_Message">Would you like to share this recipe?\n\n%1$s</string>
    <string name="Main_Share_Need_Write_External_Storage_Permission_Title">This app needs storage access</string>
    <string name="Main_Share_Need_Write_External_Storage_Permission_Message">In order to share a recipe, you need to grant this app storage access</string>
    <string name="Main_No_Internet_Title">No internet connection</string>
    <string name="Main_No_Internet_Message">In order to receive E-Mail notifications this app needs an internet connection. Please make sure that your device has a stable internet connection and restart this app.\n\nIn case you don\'t require E-Mail notifications, you can ignore this message</string>
    <string name="Main_Email_No_Password_Title">E-Mail error</string>
    <string name="Main_Email_No_Password_Message">No E-Mail notifications can be sent, please restart the app</string>

    <string name="Connection_Menu_Scan">Scan</string>
    <string name="Connection_Menu_Disconnect">Disconnect</string>
    <string name="Bluetooth_Disconnect_Disconnect_Title">Disconnect</string>
    <string name="Bluetooth_Disconnect_Disconnect_Message">Do you really wish to disconnect?</string>
    <string name="Bluetooth_Disconnect_Not_Connected">You are not connected to a controller</string>
    <string name="Bluetooth_Connect_Already_Connected">You are already connected</string>
    <string name="Bluetooth_Scan_Error">You can\'t discover new devices while being connected or discovering</string>
    <string name="Bluetooth_Layout_Not_Connected">Status: not connected</string>
    <string name="Bluetooth_Layout_Connected">Status: Bluetooth connected</string>
    <string name="Bluetooth_Scan_Need_Location_Permission_Title">This app needs location access</string>
    <string name="Bluetooth_Scan_Need_Location_Permission_Message">In order to scan for Bluetooth devices, we need to be granted location permission. Braumeister will not use your location for any purposes.</string>
    <string name="Bluetooth_Scan_Limited_Functionality_Title">Limited functionality</string>
    <string name="Bluetooth_Scan_Limited_Functionality_Message">Since the app doesn\'t have location access, you can not scan for Bluetooth devices</string>
    <string name="Bluetooth_Connect_Connect_Progress_Title">Connecting</string>
    <string name="Bluetooth_Connect_Connect_Progress_Message">Trying to connect…</string>
    <string name="Bluetooth_Disconnect_Disconnect_Progress_Title">Disconnecting</string>
    <string name="Bluetooth_Disconnect_Disconnect_Progress_Message">Trying to disconnect…</string>

    <string name="Wifi_Layout_Connected">Status: WiFi connected</string>
    <string name="Wifi_Layout_Not_Connected">Status: not connected</string>
    <string name="Wifi_Connect_Wrong_IP">The IP-Address you have entered is not valid</string>
    <string name="Wifi_Connect_Already_Connected">You are already connected to the controller</string>
    <string name="Wifi_Connect_Connect_Progress_Title">Connecting</string>
    <string name="Wifi_Disconnect_Disconnect_Title">Disconnect</string>
    <string name="Wifi_Disconnect_Disconnect_Message">Do you really wish to disconnect?</string>
    <string name="Wifi_Connect_Connect_Progress_Message">Trying to connect…</string>
    <string name="Wifi_Disconnect_Disconnect_Progress_Title">Disconnecting</string>
    <string name="Wifi_Disconnect_Disconnect_Progress_Message">Trying to disconnect…</string>
    <string name="Wifi_Connect_Disconnected">You have now been disconnected</string>
    <string name="Wifi_Connect_Connected">You are now connected to the controller</string>
    <string name="Wifi_Connect_Connection_Error">A connection could not be established</string>
    <string name="Wifi_Name_Hint">Name</string>
    <string name="Wifi_Address_Hint">IP-Address</string>
    <string name="Wifi_Add_Connection_Title">Add new connection</string>
    <string name="Wifi_Add_Connection_Message">Please enter a name and IP-Address</string>
    <string name="Wifi_Add_Connection_No_Name">You have to name the connection</string>
    <string name="Wifi_Add_Connection_No_Address">You have to enter an IP-Address</string>

    <string-array name="Recipe_Rests_Array">
        <item><i>β</i>-Glucanase (35°-40°)</item>
        <item>Peptidase (45°)</item>
        <item>Protease (57°)</item>
        <item><i>β</i>-Amylase (60°-65°)</item>
        <item>Single Infusion (66°-69°)</item>
        <item><i>α</i>-Amylase (70°-75°)</item>
        <item>Finish Mashing (78°)</item>
    </string-array>
    <string name="Recipe_Menu_Send">Send to controller</string>
    <string name="Recipe_Button_AddRest_Content">Add rest</string>
    <string name="Recipe_Button_RemoveRest_Content">Remove rest</string>
    <string name="Recipe_Title_Card_Mashing">Mashing</string>
    <string name="Recipe_Title_Card_Sparging">Sparging</string>
    <string name="Recipe_Title_Card_Cooking">Cooking</string>
    <string name="Recipe_Description_Card_Mashing">Add your rests here</string>
    <string name="Recipe_Description_Card_Sparging">Add your sparging amount here</string>
    <string name="Recipe_Description_Card_Cooking">Add your hopping rates here</string>
    <string name="Recipe_Content_Description_Malt">Malt</string>
    <string name="Recipe_Content_Description_Sparging">Sparging</string>
    <string name="Recipe_Content_Description_Hops">Hops</string>
    <string name="Recipe_Title_Amount_Of_Rests">Rests:</string>
    <string name="Recipe_Title_Amount_Of_Hops">Hoppings:</string>
    <string name="Recipe_Title_Mashing_Temperature">Mashing at:</string>
    <string name="Recipe_Title_Cooking_Duration_Total">Total duration:</string>
    <string name="Recipe_Title_Cooking_Duration">Cooking duration:</string>
    <string name="Recipe_Title_Sparge_Amount">Sparge amount:</string>
    <string name="Recipe_Cooking_Hopping_1">1. Hopping</string>
    <string name="Recipe_Cooking_Hopping_2">2. Hopping</string>
    <string name="Recipe_Cooking_Hopping_3">3. Hopping</string>
    <string name="Recipe_Cooking_Hopping_4">4. Hopping</string>
    <string name="Recipe_Cooking_Hopping_5">5. Hopping</string>
    <string name="Recipe_Cooking_Hopping_6">6. Hopping</string>
    <string name="Recipe_Cooking_Hopping_7">7. Hopping</string>
    <string name="Recipe_Save_Recipe_Inserted">The recipe has been inserted</string>
    <string name="Recipe_Save_Recipe_No_Name">You must enter a recipe name</string>
    <string name="Recipe_Save_Recipe_Updated">The recipe has been updated</string>
    <string name="Recipe_Save_Recipe_Error_Empty">You can not have empty textfields inside the recipe</string>
    <string name="Recipe_Save_Recipe_Error_Duplicate">You can not enter hoppings with the same cooking duration</string>
    <string name="Recipe_Save_Recipe_Error_Ascending">You can not enter hoppings with ascending cooking durations</string>
    <string name="Recipe_Save_Undo">Undo</string>
    <string name="Recipe_Delete_Title_Delete">Delete</string>
    <string name="Recipe_Delete_Message_Delete">Do you really wish to delete this recipe?\n\n%1$s</string>
    <string name="Recipe_Layout_Recipe_Name_Hint">Recipe name</string>
    <string name="Recipe_Load_Recipe_Loaded">The recipe has been loaded</string>
    <string name="Recipe_Delete_Recipe_Deleted">The recipe has been deleted</string>
    <string name="Recipe_Delete_No_Recipes">The database doesn\'t have any recipes</string>
    <string name="Recipe_Send_Send_Title">Send</string>
    <string name="Recipe_Send_Send_Message"><![CDATA[Do you really wish to send the loaded recipe to the controller?<br /><br /><h3>Caution:</h3>The controller will now start heating to the desired mashing temperature!]]></string>
    <string name="Recipe_Send_Data_Sent">The data has been sent to the controller</string>
    <string name="Recipe_Send_No_Recipe_Available">There is no recipe which can be sent</string>
    <string name="Recipe_Share_No_Recipe_Available">There is no recipe which can be shared</string>
    <string name="Recipe_Send_Recipe_Not_Complete_Title">Recipe failure</string>
    <string name="Recipe_Send_Recipe_Not_Complete_Message"><![CDATA[This recipe is not complete.<br /><br />It must contain at least the following data:<br /><br />Mashing temperature<br />2 rests<br />sparging amount<br />cooking duration<br />1 hop]]></string>
    <string name="Recipe_Send_Recipe_Not_Sent">The recipe has not yet been sent to the controller</string>
    <string name="Recipe_Add_Recipe_Already_Exists">This recipe name already exists</string>
    <string name="Recipe_Add_Recipe_Name_Title">Add new recipe</string>
    <string name="Recipe_Mashing_Rest_Hint">Comment</string>
    <string name="Recipe_Add_Recipe_Button_Text">Import .xsud file from KBH</string>
    <string name="Recipe_Add_Recipe_TextView_Text">Or enter a recipe manually</string>
    <string name="Recipe_Switch_VWH">First wort hop inclusive</string>

    <string name="Mashing_Menu_Start">Start rests</string>
    <string name="Mashing_Title_Temperatures">Temperatures</string>
    <string name="Mashing_Temperature_Current">Current:</string>
    <string name="Mashing_Temperature_Target">Target:</string>
    <string name="Mashing_NG_Temperature">Sparging temperature:</string>
    <string name="Mashing_Title_Times">Times</string>
    <string name="Mashing_Title_Cooking_time">Cooking time</string>
    <string name="Mashing_Title_Hopping_time">Hoppings</string>
    <string name="Mashing_Times_Remaining">Remaining:</string>
    <string name="Mashing_Times_Total">Total:</string>
    <string name="Mashing_Chart_No_Data">No data available</string>
    <string name="Mashing_Save_Chart_Saved">The chart has been saved</string>
    <string name="Mashing_Save_Chart_Not_Saved">You can\'t save an empty chart</string>
    <string name="Mashing_LineDataSet_Data_Name">Current - temperature (°C)</string>
    <string name="Mashing_Update_TV_Rest">. Rest</string>
    <string name="Mashing_Layout_Not_Started">Status: not started</string>
    <string name="Mashing_Layout_Started">Status: started</string>
    <string name="Mashing_Layout_Finished">Status: finished</string>
    <string name="Mashing_Start_Start_Title">Start</string>
    <string name="Mashing_Start_Start_Message">Do you really wish to start the first rest?</string>
    <string name="Mashing_Start_Started">Mashing has been started</string>
    <string name="Mashing_Mashing_Init_Started">Status: Mashing in started</string>
    <string name="Mashing_Mashing_Init_Finished">Status: Mashing in finished</string>
    <string name="Mashing_updateTVTask_Mashing_Init_Finished_Notification_Title">Mashing temperature has been reached</string>
    <string name="Mashing_updateTVTask_Mashing_Init_Finished_Notification_Message">You can now visit the mashing tab and start the first rest</string>
    <string name="Mashing_updateTVTask_Mashing_Finished_Notification_Title">Mashing has completed</string>
    <string name="Mashing_updateTVTask_Mashing_Finished_Notification_Message">You now have the option to begin with sparging</string>
    <string name="Mashing_updateTVTask_Mashing_Sensor_Fault">Sensor fault!</string>
    <string name="Mashing_updateTVTask_Mashing_HG_Sensor_Fault_Notification_Title">Sensor fault!</string>
    <string name="Mashing_updateTVTask_Mashing_HG_Sensor_Fault_Notification_Message">Something is wrong with the temperature sensor! Please check the sensor for correct functioning</string>
    <string name="Mashing_updateTVTask_Mashing_NG_Sensor_Fault_Notification_Title">Sensor fault!</string>
    <string name="Mashing_updateTVTask_Mashing_NG_Sensor_Fault_Notification_Message">Something is wrong with the temperature sensor! Please check the sensor for correct functioning</string>
    <string name="Mashing_Save_Need_Write_External_Storage_Permission_Title">This app needs storage access</string>
    <string name="Mashing_Save_Need_Write_External_Storage_Permission_Message">In order to save the chart, you need to grant this app storage access</string>
    <string name="Mashing_Content_Description_Thermometer">Thermometer</string>
    <string name="Mashing_Content_Description_Malt">Malt</string>
    <string name="Mashing_Button_Minus_Minutes">- 5 Min</string>
    <string name="Mashing_Button_Plus_Minutes">+ 5 Min</string>

    <string name="Sparging_Menu_Start">Start sparging</string>
    <string name="Sparging_Title_Temperature">Temperature</string>
    <string name="Sparging_Title_Amount">Sparging amount</string>
    <string name="Sparging_Layout_Not_Started">Status: not started</string>
    <string name="Sparging_Temperature_Current">Current:</string>
    <string name="Sparging_Temperature_Target">Target:</string>
    <string name="Sparging_Amount_Current">Current:</string>
    <string name="Sparging_Amount_Target">Target:</string>
    <string name="Sparging_updateTVTask_Sparging_Finished_Notification_Title">Sparging has completed</string>
    <string name="Sparging_updateTVTask_Sparging_Finished_Notification_Message">The entire sparge amount has been added. Once the lautering is completed, you may start cooking</string>
    <string name="Sparging_Start_Start_Title">Start</string>
    <string name="Sparging_Start_Start_Message">Do you really wish to start sparging?</string>
    <string name="Sparging_Start_Started">Sparging has been started</string>
    <string name="Sparging_Layout_Started">Status: started</string>
    <string name="Sparging_Layout_Finished">Status: finished</string>
    <string name="Sparging_updateTVTask_VWH_Notification_Title">First wort hop</string>
    <string name="Sparging_updateTVTask_VWH_Notification_Message">Please add the first wort hop to the wort tun now</string>

    <string name="Cooking_Menu_Start">Start cooking</string>
    <string name="Cooking_Layout_Not_Started">Status: not started</string>
    <string name="Cooking_Layout_Finished">Status: finished</string>
    <string name="Cooking_Start_Start_Title">Start</string>
    <string name="Cooking_Start_Start_Message">Do you really wish to start cooking?</string>
    <string name="Cooking_Start_Started">Cooking has been started</string>
    <string name="Cooking_Update_TV_Hopping">. Hopping</string>
    <string name="Cooking_updateTVTask_Cooking_Finished_Notification_Title">Cooking has completed</string>
    <string name="Cooking_updateTVTask_Cooking_Finished_Notification_Message">Congratulations! Your wort is now finished</string>
    <string name="Cooking_updateTVTask_Hopping_Notification_Title">. Hopping</string>
    <string name="Cooking_updateTVTask_Hopping_Notification_Message">Please add the hops to the wort kettle now</string>
    <string name="Cooking_Hopping_Time_Left">Time until next hopping:</string>
    <string name="Cooking_Content_Description_Temperature">Thermometer</string>
    <string name="Cooking_Content_Description_Water">Water</string>
    <string name="Cooking_Content_Description_Hops">Hops</string>

    <string name="Settings_Menu_Update">Update</string>
    <string name="Settings_Sensor_Fault">Sensor fault!</string>
    <string name="Settings_Title_Temperature">Temperatures</string>
    <string name="Settings_Title_Email">E-Mail Notifications</string>
    <string name="Settings_Switch_Enable">Enable</string>
    <string name="Settings_Email_Hint">Your E-Mail address</string>
    <string name="Settings_Email_Button_Ok">OK</string>
    <string name="Settings_Title_Hysteresis">Hysteresis</string>
    <string name="Settings_Title_Pump_Time">Pumping duration</string>
    <string name="Settings_Sensor_One">Hauptguss:</string>
    <string name="Settings_Sensor_Two">Nachguss:</string>
    <string name="Settings_Sensor_Two_Wait">Nachguss (Wait time):</string>
    <string name="Settings_New">New:</string>
    <string name="Settings_NA">N/A</string>
    <string name="Settings_Saved">The settings have been saved</string>
    <string name="Settings_Email_Saved">The E-Mail Address has been saved</string>
    <string name="Settings_Invalid_Email">This E-Mail Address is invalid</string>
    <string name="Settings_Content_Description_Email">E-mail</string>

    <string name="Debugging_Finish_Mashing">Finish Mashing</string>
    <string name="Debugging_Finish_Sparging">Finish Sparging</string>
    <string name="Debugging_Finish_Cooking">Finish Cooking</string>
</resources>